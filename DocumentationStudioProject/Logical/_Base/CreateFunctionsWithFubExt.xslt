<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="2.0">
  <xsl:param name="Order"></xsl:param>
  <xsl:param name="BaseFolder"></xsl:param>
  <xsl:param name="XSLTFolder"></xsl:param>
    <xsl:output encoding="ASCII" omit-xml-declaration="yes" indent="yes" name="html" version="5.0"/>
    <xsl:template match="/">
    <xsl:for-each select="Library/Library_Functions/File">
      <xsl:variable name="LibName" select="@Name" />  
        <xsl:for-each select="Function">
          <!--Check if Cyclic section should exist-->
          <xsl:variable name="IsCyclic">
            <xsl:for-each select="Members/Member">
              <xsl:choose>
                <xsl:when test= "contains(Comment3,'#CYC')">
                  <xsl:value-of select="True"/>
                </xsl:when>
              </xsl:choose>
            </xsl:for-each>
          </xsl:variable>
          <!--Check if Command section should exist-->
                 
          <xsl:choose>
            <xsl:when test="contains(@Comment3,'#OMIT')">
            </xsl:when>
            <xsl:otherwise>
              <xsl:variable name="filename" select="concat(@Name,'.html')" />
              <xsl:value-of select="$filename" />
              <!-- Creating  -->
              <xsl:result-document  href= "{$filename}"  format="html"  version="5.0">
                <html>
                  <head>
                    <TITLE>
                      <xsl:value-of select="@Name"/>
                    </TITLE>
                    <meta name="ORDER" content="{$Order}"></meta>
                    <xsl:for-each select="/Library/Library_Settings/CSS_Files/CSS_File">
                      <xsl:variable name="NameVar" select="Name"></xsl:variable>
                      <LINK REL="StyleSheet" HREF="{$NameVar}"></LINK>
                    </xsl:for-each>
                    <SCRIPT language="JavaScript" SRC="_content_folding.js"></SCRIPT>

                  </head>
                  <body>
                    <xsl:variable name="GlobalHeaderFile" select="concat(concat(concat($BaseFolder,'/'),'Global_Header'),'.html')"/>
                    <xsl:if test="doc-available($GlobalHeaderFile)">
                      <xsl:copy-of select="document($GlobalHeaderFile)" />
                    </xsl:if>
                    <h1>
                      <xsl:value-of select="@Name"/>
                    </h1>
                    <xsl:variable name="DescFile" select="concat(concat(concat(concat(concat($BaseFolder,'\'),concat('_',$LibName)),'\Descriptions\'),@Name),'_Description.html')" />
                    <xsl:choose>
                      <xsl:when test="doc-available($DescFile)">
                        <div id="sectionDESC" style="display:inline">
                          <p align="justify">
                            <xsl:copy-of select="document($DescFile)" disable-output-escaping="yes" />
                          </p>
                        </div>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="@Comment" disable-output-escaping="yes"/>
                      </xsl:otherwise>
                    </xsl:choose>
                                      
                    <p onClick="collapse_expand('section1','imgCOL_FB','imgEXP_FB')">
                      <u>
                        <font color="#3333CC" >Function Block</font>
                      </u>
                      <img id="imgEXP_FB" src="_images/_expand.png" border="0" width="12" height="11" style="display:none"/>
                      <img id="imgCOL_FB" src="_images/_collapse.png" border="0" width="12" height="11" style="display:inline" />
                    </p>
                    
                    <div id="section1" style="display:inline">
                      <p align="center">
                        <p onClick="checkbox('fubWithoutOption','fubWithOption','tableWithoutOption','tableWithOption','imgCHE_FB','imgUNC_FB')">

                          <img id="imgCHE_FB" src="_images/_checked.png" border="0" width="16" height="16" style="display:none"/>
                          <img id="imgUNC_FB" src="_images/_unchecked.png" border="0" width="16" height="16" style="display:inline" />
                          Optional parameters
                        </p>
                 <!--table for show without option-->
                        <table id="fubWithoutOption" cellspacing="0" class="fubLayout" style="display:inline">
                              <tr>
                                <td>
                                  <font color="white">1</font>
                                </td>
                                <td class="fubTop">
                                  <font color="white">1</font>
                                </td>
                                <td class="fubTop">
                                  <font color="white">1</font>
                                </td>
                                <td>
                                  <font color="white">1</font>
                                </td>
                              </tr>
                              <th class="fubDataTypeIn"> <font color="white">1</font></th>
		                            <th colspan="2" class="fubGradBlue">
                                  <xsl:value-of select="@Name"/>
                                </th>
		                            <th class="fubDataTypeOut" ><font color="white">1</font></th>
                          <!--<xsl:choose>
                            <xsl:when test="$ispar='true'">-->
                              <tr>
		                            <td class="fubDataTypeIn">
                                  <!-- This is section for data types of parameters-->
			                            <table class="fubData">
                                  <xsl:for-each select="Members/Member">
                                    <xsl:choose>
                                      <xsl:when test= "IO = 'IN' and contains(Comment3,'#PAR')and not(contains(Comment3,'#OPT')) ">                                     
                                        <tr>
                                          <td class="fubElementRight">
                                            <xsl:choose>
                                              <xsl:when test="@IsReference = 1">
                                                REFERENCE TO
                                              </xsl:when>
                                            </xsl:choose>
                                            <xsl:choose>
                                              <xsl:when test="@IsArray= 1">
                                                <xsl:choose>
                                                  <xsl:when test="@LowIsConstant = 1">
                                                    <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                    ARRAY[<a href="../Data types and constants/Constants.html">
                                                      <xsl:value-of select="@LowArrayIndex"/>
                                                    </a>..
                                                  </xsl:when>
                                                  <xsl:when test="@LowIsConstant = 0">
                                                    ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                  </xsl:when>
                                                </xsl:choose>
                                                <xsl:choose>
                                                  <xsl:when test="@HighIsConstant = 1">
                                                    <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                    <a href="../Data types and constants/Constants.html">
                                                      <xsl:value-of select="@HighArrayIndex"/>
                                                    </a>]
                                                  </xsl:when>
                                                  <xsl:when test="@HighIsConstant = 0">
                                                    <xsl:value-of select="@HighArrayIndex"/>]
                                                  </xsl:when>
                                                </xsl:choose>
                                                OF
                                              </xsl:when>
                                            </xsl:choose>
                                            <xsl:choose>
                                              <xsl:when test="@IsLibraryType = 1">
                                                <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                <xsl:choose>
                                                  <xsl:when test="@Type = 'STRUCT'">
                                                    <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                    <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                      <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                    </a>
                                                  </xsl:when>
                                                  <xsl:when test="@Type = 'ENUM'">
                                                    <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                    <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                      <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                    </a>
                                                  </xsl:when>
                                                  <xsl:when test="@Type = 'DERIVED'">
                                                    <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                      <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                    </a>
                                                  </xsl:when>
                                                  <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                    <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                    <a href="{$DataTypeVar}.html">
                                                      <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                    </a>
                                                  </xsl:when>
                                                </xsl:choose>
                                              </xsl:when>
                                              <xsl:when test="@IsString = 1">
                                                <xsl:choose>
                                                  <xsl:when test="@LowIsConstant = 1">
                                                    <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                    STRING[<a href="../Data types and constants/Constants.html">
                                                      <xsl:value-of select="@LowArrayIndex"/>
                                                    </a>]
                                                  </xsl:when>
                                                  <xsl:when test="@LowIsConstant = 0">
                                                    STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                  </xsl:when>
                                                </xsl:choose>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </td>
                                          <td width="10">
                                            <hr width="20px" size="1" color="#000000" align="rigth" />
                                          </td>
                                        </tr>
                                      </xsl:when>
                                    </xsl:choose>
                                  </xsl:for-each>
			                            </table>
		                            </td>
		                            <td class="fubGradOrange1">
			                            <table class="fubIn">
                                    <!-- This is section for parameter inputs-->
                                    <xsl:for-each select="Members/Member">
                                      <xsl:choose>
                                        <xsl:when test= "IO = 'IN' and contains(Comment3,'#PAR')and not(contains(Comment3,'#OPT')) ">
                                          <tr>
                                            <td class="fubElementLeft">
                                              <xsl:value-of select="Name"/>
                                            </td>
                                          </tr>
                                        </xsl:when>
                                      </xsl:choose>
                                    </xsl:for-each>
			                            </table>
		                            </td>
		                            <td class="fubGradOrange2">
			                            <table class="fubOut">
                                    <!-- This is section for status outupts-->
                                     <xsl:for-each select="Members/Member">
                                      <xsl:choose>
                                        <xsl:when test= "IO = 'OUT' and contains(Comment3,'#PAR')and not(contains(Comment3,'#OPT')) ">
                                          <tr>
                                            <td align="right" class="fubElementRight">
                                              <xsl:value-of select="Name"/>
                                            </td>
                                          </tr>
                                        </xsl:when>
                                      </xsl:choose>
                                    </xsl:for-each>
			                            </table>
		                            </td>
		                            <td class="fubDataTypeOut">
			                            <table class="fubData" >
                                    <!-- This is section for data types of statuses-->
                                      <xsl:for-each select="Members/Member">
                                        <xsl:choose>
                                          <xsl:when test= "IO = 'OUT' and contains(Comment3,'#PAR')and not(contains(Comment3,'#OPT')) ">
                                            <tr>
                                              <td width="10">
                                                <hr width="20px" size="1" color="#000000" align="left" />
                                              </td>
                                              <td class="fubElementLeft">
                                                <xsl:choose>
                                                  <xsl:when test="@IsReference = 1">
                                                    REFERENCE TO
                                                  </xsl:when>
                                                </xsl:choose>
                                                <xsl:choose>
                                                  <xsl:when test="@IsArray= 1">
                                                    <xsl:choose>
                                                      <xsl:when test="@LowIsConstant = 1">
                                                        <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                        ARRAY[<a href="../Data types and constants/Constants.html">
                                                          <xsl:value-of select="@LowArrayIndex"/>
                                                        </a>..
                                                      </xsl:when>
                                                      <xsl:when test="@LowIsConstant = 0">
                                                        ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                      </xsl:when>
                                                    </xsl:choose>
                                                    <xsl:choose>
                                                      <xsl:when test="@HighIsConstant = 1">
                                                        <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                        <a href="../Data types and constants/Constants.html">
                                                          <xsl:value-of select="@HighArrayIndex"/>
                                                        </a>]
                                                      </xsl:when>
                                                      <xsl:when test="@HighIsConstant = 0">
                                                        <xsl:value-of select="@HighArrayIndex"/>]
                                                      </xsl:when>
                                                    </xsl:choose>
                                                    OF
                                                  </xsl:when>
                                                </xsl:choose>
                                                <xsl:choose>
                                                  <xsl:when test="@IsLibraryType = 1">
                                                    <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                    <xsl:choose>
                                                      <xsl:when test="@Type = 'STRUCT'">
                                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                        <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                      <xsl:when test="@Type = 'ENUM'">
                                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                        <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                      <xsl:when test="@Type = 'DERIVED'">
                                                        <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                      <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                        <a href="{$DataTypeVar}.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                    </xsl:choose>
                                                  </xsl:when>
                                                  <xsl:when test="@IsString = 1">
                                                    <xsl:choose>
                                                      <xsl:when test="@LowIsConstant = 1">
                                                        <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                        STRING[<a href="../Data types and constants/Constants.html">
                                                          <xsl:value-of select="@LowArrayIndex"/>
                                                        </a>]
                                                      </xsl:when>
                                                      <xsl:when test="@LowIsConstant = 0">
                                                        STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                      </xsl:when>
                                                    </xsl:choose>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                              </td>

                                            </tr>
                                          </xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
			                            </table>
		                            </td>
	                            </tr>
                                               
	                            <tr>
		                            <td class="fubDataTypeIn">
                                  <!-- This is section for data types of cyclic inputs-->
                                  <table class="fubData"  >
                                     <xsl:for-each select="Members/Member">
                                      <xsl:choose>
                                        <xsl:when test= "IO = 'IN' and contains(Comment3,'#CYC')and not(contains(Comment3,'#OPT')) ">
                                          <tr>
                                            <td class="fubElementRight">
                                              <xsl:choose>
                                                <xsl:when test="@IsReference = 1">
                                                  REFERENCE TO
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@IsArray= 1">
                                                  <xsl:choose>
                                                    <xsl:when test="@LowIsConstant = 1">
                                                      <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                      ARRAY[<a href="../Data types and constants/Constants.html">
                                                        <xsl:value-of select="@LowArrayIndex"/>
                                                      </a>..
                                                    </xsl:when>
                                                    <xsl:when test="@LowIsConstant = 0">
                                                      ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                    </xsl:when>
                                                  </xsl:choose>
                                                  <xsl:choose>
                                                    <xsl:when test="@HighIsConstant = 1">
                                                      <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                      <a href="../Data types and constants/Constants.html">
                                                        <xsl:value-of select="@HighArrayIndex"/>
                                                      </a>]
                                                    </xsl:when>
                                                    <xsl:when test="@HighIsConstant = 0">
                                                      <xsl:value-of select="@HighArrayIndex"/>]
                                                    </xsl:when>
                                                  </xsl:choose>
                                                  OF
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@IsLibraryType = 1">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <xsl:choose>
                                                    <xsl:when test="@Type = 'STRUCT'">
                                                      <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                      <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                        <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                      </a>
                                                    </xsl:when>
                                                    <xsl:when test="@Type = 'ENUM'">
                                                      <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                      <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                        <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                      </a>
                                                    </xsl:when>
                                                    <xsl:when test="@Type = 'DERIVED'">
                                                      <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                        <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                      </a>
                                                    </xsl:when>
                                                    <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                      <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                      <a href="{$DataTypeVar}.html">
                                                        <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                      </a>
                                                    </xsl:when>
                                                  </xsl:choose>
                                                </xsl:when>
                                                <xsl:when test="@IsString = 1">
                                                  <xsl:choose>
                                                    <xsl:when test="@LowIsConstant = 1">
                                                      <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                      STRING[<a href="../Data types and constants/Constants.html">
                                                        <xsl:value-of select="@LowArrayIndex"/>
                                                      </a>]
                                                    </xsl:when>
                                                    <xsl:when test="@LowIsConstant = 0">
                                                      STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                    </xsl:when>
                                                  </xsl:choose>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                </xsl:otherwise>
                                              </xsl:choose>
                                              </td>
                                              <td width="10">
                                                <hr width="20px" size="1" color="#000000" align="rigth" />
                                              </td>
                                            </tr>
                                          </xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
			                            </table>
		                            </td>
		                            <td class="fubGradGrey1">
			                            <table class="fubIn">
                                    <!-- This is section for cyclic inputs-->
                                     <xsl:for-each select="Members/Member">
                                      <xsl:choose>
                                        <xsl:when test= "IO = 'IN' and contains(Comment3,'#CYC')and not(contains(Comment3,'#OPT')) ">
                                          <tr>
                                            <td class="fubElementLeft">
                                              <xsl:value-of select="Name"/>
                                            </td>
                                          </tr>
                                        </xsl:when>
                                      </xsl:choose>
                                    </xsl:for-each>
			                            </table>
		                            </td>
		                            <td class="fubGradGrey2">
			                            <table class="fubOut">
                                    <!-- This is section for cyclic outputs-->
                                      <xsl:for-each select="Members/Member">
                                        <xsl:choose>
                                          <xsl:when test= "IO = 'OUT' and contains(Comment3,'#CYC')and not(contains(Comment3,'#OPT')) ">
                                            <tr>
                                              <td class="fubElementRight">
                                                <xsl:value-of select="Name"/>
                                              </td>
                                            </tr>
                                          </xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
			                            </table>
		                            </td>
		                            <td class="fubDataTypeOut">
			                            <table class="fubData" >
                                    <!-- This is section for data types of cyclic outputs-->
                                      <xsl:for-each select="Members/Member">
                                        <xsl:choose>
                                          <xsl:when test= "IO = 'OUT' and contains(Comment3,'#CYC') and not(contains(Comment3,'#OPT')) ">
                                            <tr>
                                              <td width="10">
                                                <hr width="20px" size="1" color="#000000" align="left" />
                                              </td>
                                              <td class="fubElementLeft">
                                                <xsl:choose>
                                                  <xsl:when test="@IsReference = 1">
                                                    REFERENCE TO
                                                  </xsl:when>
                                                </xsl:choose>
                                                <xsl:choose>
                                                  <xsl:when test="@IsArray= 1">
                                                    <xsl:choose>
                                                      <xsl:when test="@LowIsConstant = 1">
                                                        <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                        ARRAY[<a href="../Data types and constants/Constants.html">
                                                          <xsl:value-of select="@LowArrayIndex"/>
                                                        </a>..
                                                      </xsl:when>
                                                      <xsl:when test="@LowIsConstant = 0">
                                                        ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                      </xsl:when>
                                                    </xsl:choose>
                                                    <xsl:choose>
                                                      <xsl:when test="@HighIsConstant = 1">
                                                        <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                        <a href="../Data types and constants/Constants.html">
                                                          <xsl:value-of select="@HighArrayIndex"/>
                                                        </a>]
                                                      </xsl:when>
                                                      <xsl:when test="@HighIsConstant = 0">
                                                        <xsl:value-of select="@HighArrayIndex"/>]
                                                      </xsl:when>
                                                    </xsl:choose>
                                                    OF
                                                  </xsl:when>
                                                </xsl:choose>
                                                <xsl:choose>
                                                  <xsl:when test="@IsLibraryType = 1">
                                                    <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                    <xsl:choose>
                                                      <xsl:when test="@Type = 'STRUCT'">
                                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                        <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                      <xsl:when test="@Type = 'ENUM'">
                                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                        <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                      <xsl:when test="@Type = 'DERIVED'">
                                                        <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                      <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                        <a href="{$DataTypeVar}.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                    </xsl:choose>
                                                  </xsl:when>
                                                  <xsl:when test="@IsString = 1">
                                                    <xsl:choose>
                                                      <xsl:when test="@LowIsConstant = 1">
                                                        <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                        STRING[<a href="../Data types and constants/Constants.html">
                                                          <xsl:value-of select="@LowArrayIndex"/>
                                                        </a>]
                                                      </xsl:when>
                                                      <xsl:when test="@LowIsConstant = 0">
                                                        STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                      </xsl:when>
                                                    </xsl:choose>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                              </td>

                                            </tr>
                                          </xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
			                            </table>
		                            </td>
	                            </tr>
                                <!--</xsl:when>    
                              </xsl:choose>
                          <xsl:choose>
                            <xsl:when test="$IsCmd='True'">-->
                              <tr>
		                            <td class="fubDataTypeIn">
                                    <!-- This is section for data types of commands-->
                                  <table class="fubData">
                                      <xsl:for-each select="Members/Member">
                                        <xsl:choose>
                                          <xsl:when test= "IO = 'IN' and contains(Comment3,'#CMD') and not(contains(Comment3,'#OPT')) ">
                                            <tr>
                                              <td class="fubElementRight">
                                                <xsl:choose>
                                                  <xsl:when test="@IsReference = 1">
                                                    REFERENCE TO
                                                  </xsl:when>
                                                </xsl:choose>
                                                <xsl:choose>
                                                  <xsl:when test="@IsArray= 1">
                                                    <xsl:choose>
                                                      <xsl:when test="@LowIsConstant = 1">
                                                        <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                        ARRAY[<a href="../Data types and constants/Constants.html">
                                                          <xsl:value-of select="@LowArrayIndex"/>
                                                        </a>..
                                                      </xsl:when>
                                                      <xsl:when test="@LowIsConstant = 0">
                                                        ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                      </xsl:when>
                                                    </xsl:choose>
                                                    <xsl:choose>
                                                      <xsl:when test="@HighIsConstant = 1">
                                                        <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                        <a href="../Data types and constants/Constants.html">
                                                          <xsl:value-of select="@HighArrayIndex"/>
                                                        </a>]
                                                      </xsl:when>
                                                      <xsl:when test="@HighIsConstant = 0">
                                                        <xsl:value-of select="@HighArrayIndex"/>]
                                                      </xsl:when>
                                                    </xsl:choose>
                                                    OF
                                                  </xsl:when>
                                                </xsl:choose>
                                                <xsl:choose>
                                                  <xsl:when test="@IsLibraryType = 1">
                                                    <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                    <xsl:choose>
                                                      <xsl:when test="@Type = 'STRUCT'">
                                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                        <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                      <xsl:when test="@Type = 'ENUM'">
                                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                        <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                      <xsl:when test="@Type = 'DERIVED'">
                                                        <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                      <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                        <a href="{$DataTypeVar}.html">
                                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                        </a>
                                                      </xsl:when>
                                                    </xsl:choose>
                                                  </xsl:when>
                                                  <xsl:when test="@IsString = 1">
                                                    <xsl:choose>
                                                      <xsl:when test="@LowIsConstant = 1">
                                                        <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                        STRING[<a href="../Data types and constants/Constants.html">
                                                          <xsl:value-of select="@LowArrayIndex"/>
                                                        </a>]
                                                      </xsl:when>
                                                      <xsl:when test="@LowIsConstant = 0">
                                                        STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                      </xsl:when>
                                                    </xsl:choose>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                              </td>
                                              <td width="10">
                                                <hr width="20px" size="1" color="#000000" align="rigth" />
                                              </td>
                                            </tr>
                                          </xsl:when>
                                        </xsl:choose>
                                      </xsl:for-each>
			                            </table>
		                            </td>
		                            <td class="fubGradWhite1">
			                            <table class="fubIn">
                                    <!-- This is section for command inputs-->
                                    <xsl:for-each select="Members/Member">
                                      <xsl:choose>
                                        <xsl:when test= "IO = 'IN' and contains(Comment3,'#CMD') and not(contains(Comment3,'#OPT')) ">
                                          <tr>
                                            <td class="fubElementLeft">
                                              <xsl:value-of select="Name"/>
                                            </td>
                                          </tr>
                                        </xsl:when>
                                      </xsl:choose>
                                    </xsl:for-each>
			                            </table>
		                            </td>
		                            <td class="fubGradWhite2">
			                            <table class="fubOut">
                                    <!-- This is section for additional putputs -->
                                    <xsl:for-each select="Members/Member">
                                      <xsl:choose>
                                        <xsl:when test= "IO = 'OUT' and contains(Comment3,'#CMD') and not(contains(Comment3,'#OPT')) ">
                                          <tr>
                                            <td class="fubElementRight">
                                              <xsl:value-of select="Name"/>
                                            </td>
                                          </tr>
                                        </xsl:when>
                                      </xsl:choose>
                                    </xsl:for-each>

			                            </table>
		                            </td>
		                            <td class="fubDataTypeOut">
                                    <!-- This is section for data types of outputs-->
                                    <table class="fubData">
                                      <xsl:for-each select="Members/Member">
                                      <xsl:choose>
                                        <xsl:when test= "IO = 'OUT' and contains(Comment3,'#CMD')and not(contains(Comment3,'#OPT')) ">
                                          <tr>
                                            <td width="10">
                                              <hr width="20px" size="1" color="#000000" align="left" />
                                            </td>
                                            <td class="fubElementLeft">
                                              <xsl:choose>
                                                <xsl:when test="@IsReference = 1">
                                                  REFERENCE TO
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@IsArray= 1">
                                                  <xsl:choose>
                                                    <xsl:when test="@LowIsConstant = 1">
                                                      <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                      ARRAY[<a href="../Data types and constants/Constants.html">
                                                        <xsl:value-of select="@LowArrayIndex"/>
                                                      </a>..
                                                    </xsl:when>
                                                    <xsl:when test="@LowIsConstant = 0">
                                                      ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                    </xsl:when>
                                                  </xsl:choose>
                                                  <xsl:choose>
                                                    <xsl:when test="@HighIsConstant = 1">
                                                      <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                      <a href="../Data types and constants/Constants.html">
                                                        <xsl:value-of select="@HighArrayIndex"/>
                                                      </a>]
                                                    </xsl:when>
                                                    <xsl:when test="@HighIsConstant = 0">
                                                      <xsl:value-of select="@HighArrayIndex"/>]
                                                    </xsl:when>
                                                  </xsl:choose>
                                                  OF
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@IsLibraryType = 1">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <xsl:choose>
                                                    <xsl:when test="@Type = 'STRUCT'">
                                                      <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                      <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                        <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                      </a>
                                                    </xsl:when>
                                                    <xsl:when test="@Type = 'ENUM'">
                                                      <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                      <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                        <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                      </a>
                                                    </xsl:when>
                                                    <xsl:when test="@Type = 'DERIVED'">
                                                      <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                        <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                      </a>
                                                    </xsl:when>
                                                    <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                      <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                      <a href="{$DataTypeVar}.html">
                                                        <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                      </a>
                                                    </xsl:when>
                                                  </xsl:choose>
                                                </xsl:when>
                                                <xsl:when test="@IsString = 1">
                                                  <xsl:choose>
                                                    <xsl:when test="@LowIsConstant = 1">
                                                      <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                      STRING[<a href="../Data types and constants/Constants.html">
                                                        <xsl:value-of select="@LowArrayIndex"/>
                                                      </a>]
                                                    </xsl:when>
                                                    <xsl:when test="@LowIsConstant = 0">
                                                      STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                    </xsl:when>
                                                  </xsl:choose>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                </xsl:otherwise>
                                              </xsl:choose>
                                            </td>

                                          </tr>
                                        </xsl:when>
                                      </xsl:choose>
                                    </xsl:for-each>
			                            </table>
		                            </td>
	                            </tr>
                            <!--</xsl:when>
                          </xsl:choose>-->
                              <tr>
                                <td>
                                  <font color="white">1</font>
                                </td>
                                <td class="fubBottom">
                                  <font color="white">1</font>
                                </td>
                                <td class="fubBottom">
                                  <font color="white">1</font>
                                </td>
                                <td>
                                  <font color="white">1</font>
                                </td>
                              </tr>
                            </table>
                      


                        <!--table for show with option-->
                        <table id="fubWithOption" cellspacing="0" class="fubLayout" style="display:none">
                          <tr>
                            <td>
                              <font color="white">1</font>
                            </td>
                            <td class="fubTop">
                              <font color="white">1</font>
                            </td>
                            <td class="fubTop">
                              <font color="white">1</font>
                            </td>
                            <td>
                              <font color="white">1</font>
                            </td>
                          </tr>
                          <th class="fubDataTypeIn">
                            <font color="white">1</font>
                          </th>
                          <th colspan="2" class="fubGradBlue">
                            <xsl:value-of select="@Name"/>
                          </th>
                          <th class="fubDataTypeOut" >
                            <font color="white">1</font>
                          </th>
                          <!--<xsl:choose>
                            <xsl:when test="$IsPar='True'">-->
                              <tr>
                            <td class="fubDataTypeIn">
                              <!-- This is section for data types of parameters-->
                              <table class="fubData">
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'IN' and contains(Comment3,'#PAR') ">
                                      <tr>
                                        <td class="fubElementRight">
                                          <xsl:choose>
                                            <xsl:when test="@IsReference = 1">
                                              REFERENCE TO
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsArray= 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  ARRAY[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>..
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@HighIsConstant = 1">
                                                  <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                  <a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@HighArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@HighIsConstant = 0">
                                                  <xsl:value-of select="@HighArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                              OF
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsLibraryType = 1">
                                              <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                              <xsl:choose>
                                                <xsl:when test="@Type = 'STRUCT'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'ENUM'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'DERIVED'">
                                                  <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:when test="@IsString = 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  STRING[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>
                                        <td width="10">
                                          <hr width="20px" size="1" color="#000000" align="rigth" />
                                        </td>
                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                            <td class="fubGradOrange1">
                              <table class="fubIn">
                                <!-- This is section for parameter inputs-->
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'IN' and contains(Comment3,'#PAR') ">
                                      <tr>
                                        <td class="fubElementLeft">
                                          <xsl:choose>
                                            <xsl:when test="contains(Comment3,'#OPT')">
                                              <img src="_images/_option.png" style="margin-right: 5px" width="12" height="12" />
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:value-of select="Name"/>
                                        </td>
                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                            <td class="fubGradOrange2">
                              <table class="fubOut">
                                <!-- This is section for status outupts-->
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'OUT' and contains(Comment3,'#PAR') ">
                                      <tr>
                                        <td align="right" class="fubElementRight">
                                          <xsl:value-of select="Name"/>
                                          <xsl:choose>
                                            <xsl:when test="contains(Comment3,'#OPT')">
                                              <img src="_images/_option.png" style="margin-right: 5px" width="12" height="12" />
                                            </xsl:when>
                                          </xsl:choose>
                                        </td>
                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                            <td class="fubDataTypeOut">
                              <table class="fubData" >
                                <!-- This is section for data types of statuses-->
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'OUT' and contains(Comment3,'#PAR') ">
                                      <tr>
                                        <td width="10">
                                          <hr width="20px" size="1" color="#000000" align="left" />
                                        </td>
                                        <td class="fubElementLeft">
                                          <xsl:choose>
                                            <xsl:when test="@IsReference = 1">
                                              REFERENCE TO
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsArray= 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  ARRAY[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>..
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@HighIsConstant = 1">
                                                  <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                  <a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@HighArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@HighIsConstant = 0">
                                                  <xsl:value-of select="@HighArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                              OF
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsLibraryType = 1">
                                              <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                              <xsl:choose>
                                                <xsl:when test="@Type = 'STRUCT'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'ENUM'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'DERIVED'">
                                                  <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:when test="@IsString = 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  STRING[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>

                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                          </tr>                         
                          <!--</xsl:when>
                        </xsl:choose>-->
                          <!--<xsl:choose>
                            <xsl:when test="$IsCyclic='True'">-->
                              <tr>
                            <td class="fubDataTypeIn">
                              <!-- This is section for data types of cyclic inputs-->
                              <table class="fubData"  >
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'IN' and contains(Comment3,'#CYC') ">
                                      <tr>
                                        <td class="fubElementRight">
                                          <xsl:choose>
                                            <xsl:when test="@IsReference = 1">
                                              REFERENCE TO
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsArray= 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  ARRAY[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>..
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@HighIsConstant = 1">
                                                  <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                  <a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@HighArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@HighIsConstant = 0">
                                                  <xsl:value-of select="@HighArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                              OF
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsLibraryType = 1">
                                              <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                              <xsl:choose>
                                                <xsl:when test="@Type = 'STRUCT'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'ENUM'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'DERIVED'">
                                                  <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:when test="@IsString = 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  STRING[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>
                                        <td width="10">
                                          <hr width="20px" size="1" color="#000000" align="rigth" />
                                        </td>
                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                            <td class="fubGradGrey1">
                              <table class="fubIn">
                                <!-- This is section for cyclic inputs-->
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'IN' and contains(Comment3,'#CYC') ">
                                      <tr>
                                        <td class="fubElementLeft">
                                          <xsl:choose>
                                            <xsl:when test="contains(Comment3,'#OPT')">
                                              <img src="_images/_option.png" style="margin-right: 5px" width="12" height="12" />
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:value-of select="Name"/>
                                        </td>
                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                            <td class="fubGradGrey2">
                              <table class="fubOut">
                                <!-- This is section for cyclic outputs-->
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'OUT' and contains(Comment3,'#CYC') ">
                                      <tr>
                                        <td class="fubElementRight">
                                          <xsl:value-of select="Name"/>
                                          <xsl:choose>
                                            <xsl:when test="contains(Comment3,'#OPT')">
                                              <img src="_images/_option.png" style="margin-right: 5px" width="12" height="12" />
                                            </xsl:when>
                                          </xsl:choose>
                                        </td>
                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                            <td class="fubDataTypeOut">
                              <table class="fubData" >
                                <!-- This is section for data types of cyclic outputs-->
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'OUT' and contains(Comment3,'#CYC') ">
                                      <tr>
                                        <td width="10">
                                          <hr width="20px" size="1" color="#000000" align="left" />
                                        </td>
                                        <td class="fubElementLeft">
                                          <xsl:choose>
                                            <xsl:when test="@IsReference = 1">
                                              REFERENCE TO
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsArray= 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  ARRAY[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>..
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@HighIsConstant = 1">
                                                  <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                  <a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@HighArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@HighIsConstant = 0">
                                                  <xsl:value-of select="@HighArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                              OF
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsLibraryType = 1">
                                              <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                              <xsl:choose>
                                                <xsl:when test="@Type = 'STRUCT'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'ENUM'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'DERIVED'">
                                                  <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:when test="@IsString = 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  STRING[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>

                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                          </tr>
                          <!--</xsl:when>
                        </xsl:choose>
                          <xsl:choose>
                            <xsl:when test="$IsCmd='True'">-->
                          <tr>
                            <td class="fubDataTypeIn">
                              <!-- This is section for data types of commands-->
                              <table class="fubData">
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'IN' and contains(Comment3,'#CMD') ">
                                      <tr>
                                        <td class="fubElementRight">
                                          <xsl:choose>
                                            <xsl:when test="@IsReference = 1">
                                              REFERENCE TO
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsArray= 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  ARRAY[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>..
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@HighIsConstant = 1">
                                                  <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                  <a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@HighArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@HighIsConstant = 0">
                                                  <xsl:value-of select="@HighArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                              OF
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsLibraryType = 1">
                                              <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                              <xsl:choose>
                                                <xsl:when test="@Type = 'STRUCT'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'ENUM'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'DERIVED'">
                                                  <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:when test="@IsString = 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  STRING[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>
                                        <td width="10">
                                          <hr width="20px" size="1" color="#000000" align="rigth" />
                                        </td>
                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                            <td class="fubGradWhite1">
                              <table class="fubIn">
                                <!-- This is section for command inputs-->
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'IN' and contains(Comment3,'#CMD') ">
                                      <tr>
                                        <td class="fubElementLeft">
                                          <xsl:choose>
                                            <xsl:when test="contains(Comment3,'#OPT')">
                                               <img src="_images/_option.png" style="margin-right: 5px" width="12" height="12" />
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:value-of select="Name"/>
                                        </td>
                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                            <td class="fubGradWhite2">
                              <table class="fubOut">
                                <!-- This is section for additional putputs -->
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'OUT' and contains(Comment3,'#CMD') ">
                                      <tr>
                                        <td class="fubElementRight">
                                          <xsl:value-of select="Name"/> 
                                          <xsl:choose>
                                            <xsl:when test="contains(Comment3,'#OPT')">
                                              <img src="_images/_option.png" style="margin-left: 5px" width="12" height="12" />
                                            </xsl:when>
                                          </xsl:choose>
                                        </td>
                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>

                              </table>
                            </td>
                            <td class="fubDataTypeOut">
                              <!-- This is section for data types of outputs-->
                              <table class="fubData">
                                <xsl:for-each select="Members/Member">
                                  <xsl:choose>
                                    <xsl:when test= "IO = 'OUT' and contains(Comment3,'#CMD') ">
                                      <tr>
                                        <td width="10">
                                          <hr width="20px" size="1" color="#000000" align="left" />
                                        </td>
                                        <td class="fubElementLeft">
                                          <xsl:choose>
                                            <xsl:when test="@IsReference = 1">
                                              REFERENCE TO
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsArray= 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  ARRAY[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>..
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                                </xsl:when>
                                              </xsl:choose>
                                              <xsl:choose>
                                                <xsl:when test="@HighIsConstant = 1">
                                                  <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                                  <a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@HighArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@HighIsConstant = 0">
                                                  <xsl:value-of select="@HighArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                              OF
                                            </xsl:when>
                                          </xsl:choose>
                                          <xsl:choose>
                                            <xsl:when test="@IsLibraryType = 1">
                                              <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                              <xsl:choose>
                                                <xsl:when test="@Type = 'STRUCT'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'ENUM'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'DERIVED'">
                                                  <a href="..\Data types and constants\Directly Derived Data Types.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                                <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                                  <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                                  <a href="{$DataTypeVar}.html">
                                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                                  </a>
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:when test="@IsString = 1">
                                              <xsl:choose>
                                                <xsl:when test="@LowIsConstant = 1">
                                                  <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                                  STRING[<a href="../Data types and constants/Constants.html">
                                                    <xsl:value-of select="@LowArrayIndex"/>
                                                  </a>]
                                                </xsl:when>
                                                <xsl:when test="@LowIsConstant = 0">
                                                  STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                                </xsl:when>
                                              </xsl:choose>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                        </td>

                                      </tr>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:for-each>
                              </table>
                            </td>
                          </tr>
                        <!--</xsl:when>
                        </xsl:choose>-->
                          <tr>
                            <td>
                              <font color="white">1</font>
                            </td>
                            <td class="fubBottom">
                              <font color="white">1</font>
                            </td>
                            <td class="fubBottom">
                              <font color="white">1</font>
                            </td>
                            <td>
                              <font color="white">1</font>
                            </td>
                          </tr>
                        </table>
                        
                        
                      </p>
                    </div>


                    <p onClick="collapse_expand('interface','imgCOL_IF','imgEXP_IF')">
                      <u>
                        <font color="#3333CC" >Interface</font>
                      </u>
                      <img id="imgEXP_IF" src="_images/_expand.png" border="0" width="12" height="11" style="display:inline"/>
                      <img id="imgCOL_IF" src="_images/_collapse.png" border="0" width="12" height="11" style="display:none"/>
                    </p>
                    <div id="interface" style="display:none" > 
                    <table id="tableWithoutOption" class="parameter_tab" border="1" style="display:inline">
                      <tr>
                        <th class="parameter_tab">
                          <div align="center">
                            <b>I/O</b>
                          </div>
                        </th>
                        <th class="parameter_tab">
                          <div align="center">
                            <b>Parameter</b>
                          </div>
                        </th>
                        <th class="parameter_tab">
                          <div align="center">
                            <b>Data Type</b>
                          </div>
                        </th>
                        <th class="parameter_tab">
                          <div align="center">
                            <b>Description</b>
                          </div>
                        </th>
                      </tr>
                      <xsl:for-each select="Members/Member">
                        <xsl:choose>
                          <xsl:when test= "IO = 'VAR' and /Library/Library_Functions/@VARMemberVisible = 0">
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:if test="not(contains(Comment3,'#OPT'))">
                              <tr>
                              <td valign="TOP" class="parameter_tab">
                                <xsl:value-of select="IO"/>
                              </td>
                              <td valign="TOP" class="parameter_tab">
                                <xsl:value-of select="Name"/>
                              </td>
                              <td valign="TOP" class="parameter_tab">
                                <xsl:choose>
                                  <xsl:when test="@IsReference = 1">
                                    REFERENCE TO
                                  </xsl:when>
                                </xsl:choose>
                                <xsl:choose>
                                  <xsl:when test="@IsArray= 1">
                                    <!--<xsl:choose>
                           <xsl:when test="@IsConstant= 1">
                             <xsl:variable name="ConstantName" select="@HighArrayIndex"></xsl:variable>
                             ARRAY[<xsl:value-of select="@LowArrayIndex"/>..<a href="../Data types and constants/Constants.html">
                             <xsl:value-of select="@HighArrayIndex" disable-output-escaping="yes"/></a>]
                           </xsl:when>
                           <xsl:otherwise>
                           ARRAY[<xsl:value-of select="@LowArrayIndex"/>..<xsl:value-of select="@HighArrayIndex" disable-output-escaping="yes"/>]
                           </xsl:otherwise>
                         </xsl:choose>-->

                                    <xsl:choose>
                                      <xsl:when test="@LowIsConstant = 1">
                                        <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                        ARRAY[<a href="../Data types and constants/Constants.html">
                                          <xsl:value-of select="@LowArrayIndex"/>
                                        </a>..
                                      </xsl:when>
                                      <xsl:when test="@LowIsConstant = 0">
                                        ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                      </xsl:when>
                                    </xsl:choose>
                                    <xsl:choose>
                                      <xsl:when test="@HighIsConstant = 1">
                                        <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                        <a href="../Data types and constants/Constants.html">
                                          <xsl:value-of select="@HighArrayIndex"/>
                                        </a>]
                                      </xsl:when>
                                      <xsl:when test="@HighIsConstant = 0">
                                        <xsl:value-of select="@HighArrayIndex"/>]
                                      </xsl:when>
                                    </xsl:choose>
                                    OF
                                  </xsl:when>
                                </xsl:choose>
                                <xsl:choose>
                                  <xsl:when test="@IsLibraryType = 1">
                                    <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                    <xsl:choose>
                                      <xsl:when test="@Type = 'STRUCT'">
                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                        <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                        </a>
                                      </xsl:when>
                                      <xsl:when test="@Type = 'ENUM'">
                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                        <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                        </a>
                                      </xsl:when>
                                      <xsl:when test="@Type = 'DERIVED'">
                                        <a href="..\Data types and constants\Directly Derived Data Types.html">
                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                        </a>
                                      </xsl:when>
                                      <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                        <a href="{$DataTypeVar}.html">
                                          <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                        </a>
                                      </xsl:when>
                                    </xsl:choose>
                                  </xsl:when>
                                  <xsl:when test="@IsString = 1">
                                    <xsl:choose>
                                      <xsl:when test="@LowIsConstant = 1">
                                        <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                        STRING[<a href="../Data types and constants/Constants.html">
                                          <xsl:value-of select="@LowArrayIndex"/>
                                        </a>]
                                      </xsl:when>
                                      <xsl:when test="@LowIsConstant = 0">
                                        STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                      </xsl:when>
                                    </xsl:choose>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </td>
                              <td valign="TOP" class="parameter_tab">
                                <xsl:value-of select="Comment" disable-output-escaping="yes" />
                              </td>
                            </tr>
                            </xsl:if>  
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:for-each>
                      <xsl:choose>
                        <xsl:when test="@Type='Function'">
                          <tr>
                            <td valign="TOP" class="parameter_tab">
                              OUT
                            </td>
                            <td valign="TOP" class="parameter_tab">
                              Return Value
                            </td>
                            <td valign="TOP" class="parameter_tab">
                              <xsl:choose>
                                <xsl:when test="@ReturnValueIsLibraryType = 1">
                                  <xsl:choose>
                                    <xsl:when test="@ReturnValueType = 'ENUM'">
                                      <xsl:variable name="DataTypeNameVar" select="@ReturnValueName"></xsl:variable>
                                      <a href="..\Data types and constants\Enumerators\{$DataTypeNameVar}.html">
                                        <xsl:value-of select="@ReturnValueName" disable-output-escaping="no"/>
                                      </a>
                                    </xsl:when>
                                  </xsl:choose>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="@ReturnValueName"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td valign="TOP" class="parameter_tab">
                              <xsl:value-of select="@ReturnValueComment" disable-output-escaping="yes" />
                            </td>
                          </tr>
                        </xsl:when>
                      </xsl:choose>
                    </table>
                      <table id="tableWithOption" class="parameter_tab" border="1" style="display:none">
                        <tr>
                          <th class="parameter_tab">
                            <div align="center">
                              <b>I/O</b>
                            </div>
                          </th>
                          <th class="parameter_tab">
                            <div align="center">
                              <b>Parameter</b>
                            </div>
                          </th>
                          <th class="parameter_tab">
                            <div align="center">
                              <b>Data Type</b>
                            </div>
                          </th>
                          <th class="parameter_tab">
                            <div align="center">
                              <b>Description</b>
                            </div>
                          </th>
                        </tr>
                        <xsl:for-each select="Members/Member">
                          <xsl:choose>
                            <xsl:when test= "IO = 'VAR' and /Library/Library_Functions/@VARMemberVisible = 0">
                            </xsl:when>
                            <xsl:otherwise>
                              
                                <tr>
                                  <td valign="TOP" class="parameter_tab">
                                    <xsl:value-of select="IO"/>
                                    <xsl:if test="contains(Comment3,'#OPT')">
                                      <img src="_images/_option.png" style="margin-left: 5px" width="12" height="12" />
                                    </xsl:if>
                                  </td>
                                  <td valign="TOP" class="parameter_tab">
                                    <xsl:value-of select="Name"/>
                                    
                                  </td>
                                  <td valign="TOP" class="parameter_tab">
                                    <xsl:choose>
                                      <xsl:when test="@IsReference = 1">
                                        REFERENCE TO
                                      </xsl:when>
                                    </xsl:choose>
                                    <xsl:choose>
                                      <xsl:when test="@IsArray= 1">
                                        <!--<xsl:choose>
                           <xsl:when test="@IsConstant= 1">
                             <xsl:variable name="ConstantName" select="@HighArrayIndex"></xsl:variable>
                             ARRAY[<xsl:value-of select="@LowArrayIndex"/>..<a href="../Data types and constants/Constants.html">
                             <xsl:value-of select="@HighArrayIndex" disable-output-escaping="yes"/></a>]
                           </xsl:when>
                           <xsl:otherwise>
                           ARRAY[<xsl:value-of select="@LowArrayIndex"/>..<xsl:value-of select="@HighArrayIndex" disable-output-escaping="yes"/>]
                           </xsl:otherwise>
                         </xsl:choose>-->

                                        <xsl:choose>
                                          <xsl:when test="@LowIsConstant = 1">
                                            <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                            ARRAY[<a href="../Data types and constants/Constants.html">
                                              <xsl:value-of select="@LowArrayIndex"/>
                                            </a>..
                                          </xsl:when>
                                          <xsl:when test="@LowIsConstant = 0">
                                            ARRAY[<xsl:value-of select="@LowArrayIndex"/>..
                                          </xsl:when>
                                        </xsl:choose>
                                        <xsl:choose>
                                          <xsl:when test="@HighIsConstant = 1">
                                            <xsl:variable name="HighConstantName" select="@HighArrayIndex"></xsl:variable>
                                            <a href="../Data types and constants/Constants.html">
                                              <xsl:value-of select="@HighArrayIndex"/>
                                            </a>]
                                          </xsl:when>
                                          <xsl:when test="@HighIsConstant = 0">
                                            <xsl:value-of select="@HighArrayIndex"/>]
                                          </xsl:when>
                                        </xsl:choose>
                                        OF
                                      </xsl:when>
                                    </xsl:choose>
                                    <xsl:choose>
                                      <xsl:when test="@IsLibraryType = 1">
                                        <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                        <xsl:choose>
                                          <xsl:when test="@Type = 'STRUCT'">
                                            <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                            <a href="..\Data types and constants\Data types\{$DataTypeVar}.html">
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </a>
                                          </xsl:when>
                                          <xsl:when test="@Type = 'ENUM'">
                                            <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                            <a href="..\Data types and constants\Enumerators\{$DataTypeVar}.html">
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </a>
                                          </xsl:when>
                                          <xsl:when test="@Type = 'DERIVED'">
                                            <a href="..\Data types and constants\Directly Derived Data Types.html">
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </a>
                                          </xsl:when>
                                          <xsl:when test="@Type = 'FUNCTION_BLOCK'">
                                            <xsl:variable name="DataTypeVar" select="DataType"></xsl:variable>
                                            <a href="{$DataTypeVar}.html">
                                              <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                            </a>
                                          </xsl:when>
                                        </xsl:choose>
                                      </xsl:when>
                                      <xsl:when test="@IsString = 1">
                                        <xsl:choose>
                                          <xsl:when test="@LowIsConstant = 1">
                                            <xsl:variable name="LowConstantName" select="@LowArrayIndex"></xsl:variable>
                                            STRING[<a href="../Data types and constants/Constants.html">
                                              <xsl:value-of select="@LowArrayIndex"/>
                                            </a>]
                                          </xsl:when>
                                          <xsl:when test="@LowIsConstant = 0">
                                            STRING[<xsl:value-of select="@LowArrayIndex"/>]
                                          </xsl:when>
                                        </xsl:choose>
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <xsl:value-of select="DataType" disable-output-escaping="no"/>
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  </td>
                                  <td valign="TOP" class="parameter_tab">
                                    <xsl:value-of select="Comment" disable-output-escaping="yes" />
                                  </td>
                                </tr>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:for-each>
                        <xsl:choose>
                          <xsl:when test="@Type='Function'">
                            <tr>
                              <td valign="TOP" class="parameter_tab">
                                OUT
                              </td>
                              <td valign="TOP" class="parameter_tab">
                                Return Value
                              </td>
                              <td valign="TOP" class="parameter_tab">
                                <xsl:choose>
                                  <xsl:when test="@ReturnValueIsLibraryType = 1">
                                    <xsl:choose>
                                      <xsl:when test="@ReturnValueType = 'ENUM'">
                                        <xsl:variable name="DataTypeNameVar" select="@ReturnValueName"></xsl:variable>
                                        <a href="..\Data types and constants\Enumerators\{$DataTypeNameVar}.html">
                                          <xsl:value-of select="@ReturnValueName" disable-output-escaping="no"/>
                                        </a>
                                      </xsl:when>
                                    </xsl:choose>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:value-of select="@ReturnValueName"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </td>
                              <td valign="TOP" class="parameter_tab">
                                <xsl:value-of select="@ReturnValueComment" disable-output-escaping="yes" />
                              </td>
                            </tr>
                          </xsl:when>
                        </xsl:choose>
                      </table>  
                    </div>    
                    <xsl:variable name="FuncFile" select="concat(concat(concat(concat(concat($BaseFolder,'\'),concat('_',$LibName)),'\Descriptions\'),@Name),'_Functionality.html')" />
                    <xsl:if test="doc-available($FuncFile)">
                    <p onClick="collapse_expand('sectionFD','imgCOL_FD','imgEXP_FD')">
                      <u>
                        <font color="#3333CC" >Functionality</font>
                      </u>
                      <img id="imgEXP_FD" src="_images/_expand.png" border="0" width="12" height="11" style="display:inline"/>
                      <img id="imgCOL_FD" src="_images/_collapse.png" border="0" width="12" height="11" style="display:none" />
                    </p>
                  
                    <div id="sectionFD" style="display:none">
                      <div id="sectionWithout">
                        <p align="justify">                   
                           <xsl:copy-of select="document($FuncFile)" />
                        </p>
                      </div>
                    </div>
                    </xsl:if>
                    <div position="fixed" bottom ="0px">
                      <xsl:variable name="GlobalFooterFile" select="concat(concat(concat($BaseFolder,'/'),'Global_Footer'),'.html')"/>
                      <xsl:if test="doc-available($GlobalFooterFile)">   
                        <br/>
                        <xsl:copy-of select="document($GlobalFooterFile)" />
                      </xsl:if>
                    </div>            
                  </body>
                </html>
              </xsl:result-document>
            </xsl:otherwise>
          </xsl:choose>        
      </xsl:for-each>
   </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>


