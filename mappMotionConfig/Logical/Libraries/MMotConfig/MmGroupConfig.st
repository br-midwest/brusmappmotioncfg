
(* Update parameters in Mapp Motion Axis Group  *)
FUNCTION_BLOCK MmGroupConfig
    
    IF NOT Update AND Intern.UpdateLast THEN
        Intern.Step := MGC_RESETTING;
    END_IF;
    
    CASE Intern.Step OF
        MGC_WAIT:
            IF Update THEN
                Busy := TRUE;
                Intern.Step := MGC_GET_AX;
            END_IF;
            		  
        MGC_GET_AX:
        
            // Do this first load to get the axis info, and axis group feature info.  Set up poiners to store info about both.
            // We don't care about the rest of configuration.  We will overwrite the data with the par struct on the input of the FUB
            
            (* first, set up the unbounded array so we can get the axis group cfg read out.  See GUID 101efa68-cee5-438d-961e-28d96f104b27 *)
            Intern.AxGrpCfgStorage.PhysicalAxes.JointAxes.JointAxis.NumberOfArrayElements := SIZEOF(Intern.JointInfoStorage)/SIZEOF(Intern.JointInfoStorage[0]);
            Intern.AxGrpCfgStorage.PhysicalAxes.JointAxes.JointAxis.DataAddress := ADR(Intern.JointInfoStorage);
            
            (* Also, set up unbounded arrays for axis group feature references.  Need to get and store to be able to write anything back in without making changes to this config... *)
            Intern.AxGrpCfgStorage.AxesGroupFeatures.FeatureReference.NumberOfArrayElements := SIZEOF(Intern.AxGrpFeatStorage)/SIZEOF(Intern.AxGrpFeatStorage[0]);
            Intern.AxGrpCfgStorage.AxesGroupFeatures.FeatureReference.DataAddress := ADR(Intern.AxGrpFeatStorage);
            
            Intern.MC_BR_ProcessConfig_0.Execute := TRUE;
            Intern.MC_BR_ProcessConfig_0.Name := GroupName;
            Intern.MC_BR_ProcessConfig_0.DataType := mcCFG_AXGRP_PATHGEN; 
            Intern.MC_BR_ProcessConfig_0.DataAddress := ADR(Intern.AxGrpCfgStorage); 
            Intern.MC_BR_ProcessConfig_0.Mode := mcPCM_LOAD;
        
            Intern.Step := MGC_WAIT_GET_AX;
            
        MGC_WAIT_GET_AX:
            IF Intern.MC_BR_ProcessConfig_0.Error THEN
                Intern.ErrorStep := Intern.Step;
                Intern.Step := MGC_ERROR;
            ELSIF Intern.MC_BR_ProcessConfig_0.Done THEN
                Intern.MC_BR_ProcessConfig_0.Execute := FALSE;
                Intern.Step := MGC_SAVE_CFG;
            END_IF;
        
        MGC_SAVE_CFG: // trigger save config
            
            (* Copy parameter struct input into library, then reuse internal joint and feature pointers from previous step to update axis group *)
            
            Intern.AxGrpCfgStorage := GroupSettings; //memcopy required? ********************************************************************************************************************************
            
            (* Reuse the joint and feature references to prevent error when writing back in.  No changes are made in this area, just echoing back same settings. *)
            Intern.AxGrpCfgStorage.PhysicalAxes.JointAxes.JointAxis.NumberOfArrayElements := SIZEOF(Intern.JointInfoStorage)/SIZEOF(Intern.JointInfoStorage[0]);
            Intern.AxGrpCfgStorage.PhysicalAxes.JointAxes.JointAxis.DataAddress := ADR(Intern.JointInfoStorage);
            Intern.AxGrpCfgStorage.AxesGroupFeatures.FeatureReference.NumberOfArrayElements := SIZEOF(Intern.AxGrpFeatStorage)/SIZEOF(Intern.AxGrpFeatStorage[0]);
            Intern.AxGrpCfgStorage.AxesGroupFeatures.FeatureReference.DataAddress := ADR(Intern.AxGrpFeatStorage);
            
            Intern.MC_BR_ProcessConfig_0.Execute := TRUE;
            Intern.MC_BR_ProcessConfig_0.Name := GroupName;
            Intern.MC_BR_ProcessConfig_0.DataType := mcCFG_AXGRP_PATHGEN; 
            Intern.MC_BR_ProcessConfig_0.DataAddress := ADR(Intern.AxGrpCfgStorage); 
            Intern.MC_BR_ProcessConfig_0.Mode := mcPCM_SAVE;
            
            Intern.Step := MGC_WAIT_SAVE_CFG;
            
        MGC_WAIT_SAVE_CFG: // wait save config done
            IF Intern.MC_BR_ProcessConfig_0.Error THEN
                Intern.ErrorStep := Intern.Step;
                Intern.Step := MGC_ERROR;
            ELSIF Intern.MC_BR_ProcessConfig_0.Done THEN
                Intern.MC_BR_ProcessConfig_0.Execute := FALSE;
                Intern.Step := MGC_WAIT_POWER_OFF;
            END_IF;
            
        MGC_WAIT_POWER_OFF: // power off group (requirement of load from config for axis group)
            
            Intern.MC_GroupReadStatus_0.Enable := TRUE;
            Intern.MC_GroupReadStatus_0.AxesGroup := ADR(GroupRef);
            IF Intern.MC_GroupReadStatus_0.Error THEN
                Intern.ErrorStep := Intern.Step;
                Intern.Step := MGC_ERROR;
            ELSIF Intern.MC_GroupReadStatus_0.Valid AND Intern.MC_GroupReadStatus_0.GroupDisabled THEN
                WaitingGroupPwrOff := FALSE;
                Intern.MC_GroupReadStatus_0.Enable := FALSE;
                Intern.Step := MGC_ACTIVATE_PARAM;
            ELSIF Intern.MC_GroupReadStatus_0.Valid AND NOT Intern.MC_GroupReadStatus_0.GroupDisabled THEN
                WaitingGroupPwrOff := TRUE;
            END_IF;
        
        MGC_ACTIVATE_PARAM:
            
            Intern.MC_BR_ProcessParam_0.Execute := TRUE; 
            Intern.MC_BR_ProcessParam_0.Component := ADR(GroupRef);
            Intern.MC_BR_ProcessParam_0.DataType := mcCFG_AXGRP_PATHGEN;
            Intern.MC_BR_ProcessParam_0.DataAddress := ADR(Intern.AxGrpCfgStorage);
            Intern.MC_BR_ProcessParam_0.Mode := mcPPM_LOAD_FROM_CONFIG;
            Intern.MC_BR_ProcessParam_0.ExecutionMode := mcEM_IMMEDIATELY;

            Intern.Step := MGC_WAIT_PARAM;
           
        MGC_WAIT_PARAM:     // wait load from config to finish          
            IF Intern.MC_BR_ProcessParam_0.Error THEN
                Intern.ErrorStep := Intern.Step;
                Intern.Step := MGC_ERROR;
            ELSIF Intern.MC_BR_ProcessParam_0.Done THEN
                Intern.Step := MGC_DONE;
                Intern.MC_BR_ProcessParam_0.Execute := FALSE;
            END_IF;
            
        MGC_DONE:
            Done := TRUE;
            Busy := FALSE;
            
        MGC_ERROR:
            Error := TRUE;
            Busy := FALSE;
            
        MGC_RESETTING:
            Intern.MC_GroupReadStatus_0.Enable      := FALSE;
            Intern.MC_BR_ProcessConfig_0.Execute    := FALSE;
            Intern.MC_BR_ProcessParam_0.Execute     := FALSE;
            Error := FALSE;
            Done := FALSE;
            Busy := FALSE;
            WaitingGroupPwrOff := FALSE;
            Intern.ErrorStep := MGC_WAIT;
            Intern.Step := MGC_WAIT;
            
    END_CASE;
    
    Intern.UpdateLast := Update;
    
    // call fub
    Intern.MC_BR_ProcessConfig_0();
    Intern.MC_BR_ProcessParam_0();
    Intern.MC_GroupReadStatus_0();
    
END_FUNCTION_BLOCK
