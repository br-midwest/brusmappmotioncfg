(*********************************************************************************
 * Copyright: B&R Industrial Automation GmbH 
 * Author:    olsonb 
 * Created:   January 27, 2022/5:44 PM 
 * 
 * A FUB that writes settings from an MpAxisBasicConfigType structure to a mappMotion
 * axis. It reads the current settings and compares them to the new settings to
 * determine if a warm restart is required. If possible, it also calls MC_BR_ProcessParam
 * to initialize any changes and bypass the neeed for a restart.
 * 
 * In order to map an axis reference to a hardware module, that reference must be
 * assigned to some module in the physical view. In other words, this FUB cannot
 * assign NEW axis references, it can only move around EXISTING axis references.
 * (An axis only counts as "existing" if it is mapped in the physical view.)
 * 
 * DANGER: If an axis reference is unmapped from all hardware modules, the next warm
 * restart will corrupt that reference until the next rebuild in Automation Studio.
 * 
 * AxisADR defines the axis reference that SofwareConfigWrite will be written to.
 * AxisLocation defines which hardware module that HardwareConfigWrite will be written to.
 * BasicConfig.AxisName defines which axis reference will be assigned to that hardware module.
 * 
 * IMPORTANT: To avoid confusion, BasicConfig.AxisName and AxisADR should always match.
 *********************************************************************************)

FUNCTION_BLOCK MmWriteAxisConfig

	IF Execute THEN
		CASE ConfigWriteState OF
	
			WRITE_WAIT: // Convert BasicConfig to mappMotion config structures
				BasicConfigToMappConfig;
				ConfigWriteState := WRITE_COMPARE;
			
			WRITE_COMPARE: // Read the current configuration for comparisons
				ReadConfig.AxisLocation	:= AxisLocation;
				ReadConfig.AxisADR		:= ADR(AxisADR);
				ReadConfig.Execute		:= TRUE;
				IF ReadConfig.Done THEN
					ReadConfig.Execute := FALSE;
					ConfigWriteState := WRITE_CONFIG;
				END_IF
			
			WRITE_CONFIG:
				// Check if a restart is required
				IF brsmemcmp(ADR(ReadConfig.SoftwareConfigRead.BaseType), ADR(SoftwareConfigWrite.BaseType), SIZEOF(SoftwareConfigWrite.BaseType)) <> 0 OR
					ReadConfig.HardwareConfigRead.AxisReference.Name <> HardwareConfigWrite.AxisReference.Name THEN
					RestartRequired := TRUE; // Request a restart
				END_IF
				
				// Copy existing configuration as a default for writing
				HardwareConfigWrite := ReadConfig.HardwareConfigRead;
				SoftwareConfigWrite := ReadConfig.SoftwareConfigRead;
				// Overwrite this with anything definied in BasicConfig
				BasicConfigToMappConfig;
				
				// HardwareConfig
				WriteConfig[0].Name			:= AxisLocation;
				WriteConfig[0].Mode			:= mcPCM_SAVE;
				WriteConfig[0].DataType		:= mcCFG_ACP_AX;
				WriteConfig[0].DataAddress	:= ADR(HardwareConfigWrite);
				WriteConfig[0].Execute		:= TRUE;
				// SoftwareConfig
				WriteConfig[1].Name			:= BasicConfig.AxisName;
				WriteConfig[1].Mode			:= mcPCM_SAVE;
				WriteConfig[1].DataType		:= mcCFG_AX;
				WriteConfig[1].DataAddress	:= ADR(SoftwareConfigWrite);
				WriteConfig[1].Execute		:= TRUE;
	
				// Wait for done
				IF WriteConfig[0].Done AND WriteConfig[1].Done THEN
					IF RestartRequired THEN	// Skip init if a restart is required anyway
							ConfigWriteState := WRITE_DONE;
						ELSE
							ConfigWriteState := WRITE_INIT;
					END_IF
				END_IF
				
			WRITE_INIT:
				InitConfig.Execute	:= TRUE;
				InitConfig.AxisADR	:= ADR(AxisADR);
				IF InitConfig.Done THEN
					ConfigWriteState := WRITE_DONE;
				END_IF
				
			WRITE_DONE:
				// Read the current configuration
				ReadConfig.Execute 		:= TRUE;
				ReadConfig.AxisLocation	:= AxisLocation;
				IF ReadConfig.Done OR Done THEN
					Done := TRUE;
					ReadConfig.Execute := FALSE;
				END_IF
			
			WRITE_ERROR:
			// Wait for Execute to be reset
		
		END_CASE
	ELSE
		ReadConfig.Execute		:= FALSE;
		WriteConfig[0].Execute	:= FALSE;
		WriteConfig[1].Execute	:= FALSE;
		InitConfig.Execute		:= FALSE;
		Done := FALSE;
		Error := FALSE;
		ConfigWriteState := WRITE_WAIT;
	END_IF

	// Call FBs
	ReadConfig();
	WriteConfig[0]();
	WriteConfig[1]();
	InitConfig();
	
	// Status Variables
	Busy := NOT (ConfigWriteState = WRITE_WAIT OR ConfigWriteState = WRITE_DONE OR ConfigWriteState = WRITE_ERROR);
	Error := ReadConfig.Error OR WriteConfig[0].Error OR WriteConfig[1].Error OR InitConfig.Error;
	Done := ConfigWriteState = WRITE_DONE AND NOT ReadConfig.Execute;
	
	(* Move to Error state *)
	IF Error THEN
		ConfigWriteState := WRITE_ERROR;
	END_IF

END_FUNCTION_BLOCK