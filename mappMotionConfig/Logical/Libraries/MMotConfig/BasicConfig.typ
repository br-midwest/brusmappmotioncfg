
TYPE
	MpAxisInputSourceEnum : 
		( (*Trigger source options list*)
		mpAXIS_INPUT_NONE := 0, (*Source input not defined (functionality not active)*)
		mpAXIS_INPUT_TRIGGER1 := 1, (*Trigger 1 as source*)
		mpAXIS_INPUT_TRIGGER2 := 2, (*Trigger 2 as source*)
		mpAXIS_INPUT_POS_LIMIT_SWITCH := 3, (*Positive limit switch as source*)
		mpAXIS_INPUT_NEG_LIMIT_SWITCH := 4, (*Negative limit switch as source*)
		mpAXIS_INPUT_HOMING_SWITCH := 5 (*Homing switch as source*)
		);
	MpAxisBasicConfigType : 	STRUCT  (*Global MpAxis configuration*)
		AxisName : STRING[80]; (*Axis Name*)
		Axis : MpAxisBasicConfigAxisType; (*Axis configuration*)
		Drive : MpAxisBasicConfigDriveType; (*Drive configuration*)
		PositionOutput : MpAxisPosVelOutputTypeEnum := mpAXIS_OUTPUT_SET; (*Defines if the value provided by Position output is the set profile generated or real motor position*)
		VelocityOutput : MpAxisPosVelOutputTypeEnum := mpAXIS_OUTPUT_SET; (*Defines if the value provided by Velocity output is the set profile generated or real motor velocity*)
	END_STRUCT;
	MpAxisBasicConfigAxisType : 	STRUCT  (*Axis configuration*)
		BaseType : MpAxisBaseTypeEnum := mpAXIS_PERIODIC_ROTARY; (*Defines whether the axis is linear or rotary. Also defines whether the axis is periodic*)
		MeasurementUnit : MpAxisMeasurementUnitEnum := mpAXIS_UNIT_DEGREES; (*Defines the unit of measurement*)
		MeasurementResolution : LREAL := 0.01; (*Minimum distance that can be used for a movement. It is designed to be configured as result from 1e(-X) where X is the exponent and represents the minimum digit (1.0, 0.1, 0.01, 0.001...)*)
		SoftwareLimitPositions : MpAxisSoftwareLimitType; (*Software position limit values*)
		PeriodSettings : MpAxisPeriodType; (*Period definition*)
		MovementLimits : MpAxisMovementLimitsType; (*Limits configuration*)
		CyclicReadChannels : MpAxisCyclicReadChannelsType; (*Channel for cyclic read a custom parameter*)
	END_STRUCT;
	MpAxisCyclicReadChannelsType : 	STRUCT 
		UserChannelParameterID : UINT := 214; (*ParID of drive that should be cyclically read (Default 214: CTRL Current controller: Actual stator current quadrature component)*)
	END_STRUCT;
	MpAxisSoftwareLimitType : 	STRUCT  (*Software position limits*)
		LowerLimit : LREAL := -8388608.0; (*Software limit value in the negative direction [units]*)
		UpperLimit : LREAL := 8388607.0; (*Software limit value in the positive direction [units]*)
	END_STRUCT;
	MpAxisPeriodType : 	STRUCT  (*Period definition*)
		Period : LREAL := 360.0; (*Motor period [units]*)
	END_STRUCT;
	MpAxisMovementLimitsType : 	STRUCT  (*MovementLimits configuration*)
		VelocityPositive : REAL := 1000.0; (*Maximum velocity for discrete movements in the positive direction [units/s]*)
		VelocityNegative : REAL := 1000.0; (*Maximum velocity for discrete movements in the negative direction [units/s]*)
		Acceleration : REAL := 5000.0; (*Maximum acceleration for discrete movements [units/s]�*)
		Deceleration : REAL := 5000.0; (*Maximum deceleration for discrete movements [units/s]�*)
		JerkTime : REAL := 0.0; (*Limit for acceleration slope [s]*)
		PositionErrorStopLimit : LREAL := 2.0; (*Maximum lag error [units]*)
		VelocityErrorStopLimit : REAL := 0.0; (*Maximum velocity [units/s]*)
		VelocityErrorStopLimitMode : MpAxisVelocityLimitModeEnum := mpAXIS_VEL_MODE_OFF; (*Velocity limit mode*)
	END_STRUCT;
	MpAxisBasicConfigDriveType : 	STRUCT  (*Drive configuration*)
		Gearbox : MpAxisGearboxType; (*Scaling configuration*)
		Transformation : MpAxisTransformationType; (*Transformation configuration for rotary and linear axes*)
		Controller : MpAxisControllerType; (*Tuning configuration*)
		StopReaction : MpAxisStopReactionType; (*Stop configuration*)
		DigitalInputs : MpAxisDigitalIOType; (*Configuration of digital inputs*)
	END_STRUCT;
	MpAxisGearboxType : 	STRUCT  (*Scaling configuration*)
		Input : UDINT := 1; (*Input ratio*)
		Output : UDINT := 1; (*Output ratio*)
		Direction : MpAxisMotorDirectionEnum := mpAXIS_DIR_CLOCKWISE; (*Direction of rotation of the motor*)
		MaximumTorque : REAL; (*Maximum torque allowed*)
	END_STRUCT;
	MpAxisTransformationType : 	STRUCT  (*Transformation configuration*)
		ReferenceDistance : LREAL := 360.0; (*Linear axis: Defines the distance moved by the linear axis while the output after the gearbox (on the load side) moves one rotation [units]
Rotary axis: Defines the physical units in relation to one rotation of the gearbox output [units]*)
	END_STRUCT;
	MpAxisControllerType : 	STRUCT  (*Controller configuration*)
		Mode : MpAxisControllerModeEnum := mpAXIS_CTRL_MODE_POSITION; (*Controller mode*)
		Position : MpAxisControllerPositionType; (*Control parameters for the position controller*)
		Speed : MpAxisControllerSpeedType; (*Control parameters for the speed controller*)
		FeedForward : MpAxisControllerFeedForwardType; (*Control parameters for the feed-forward*)
		VoltageFrequency : MpAxisControllerVoltageFreqType; (*Control parameters for the voltage frequency*)
		LoopFilters : ARRAY[0..2]OF MpAxisControllerLoopFiltersType; (*Loop filters*)
	END_STRUCT;
	MpAxisControllerPositionType : 	STRUCT  (*Controller for position loop*)
		ProportionalGain : REAL := 50.0; (*Gain factor [1/s]*)
		IntegralTime : REAL := 0.0; (*Integral time [s]*)
		PredictionTime : REAL := 0.0004; (*Prediction time [s]*)
		TotalDelayTime : REAL := 0.0004; (*Total delay time [s]*)
	END_STRUCT;
	MpAxisControllerSpeedType : 	STRUCT  (*Controller for speed loop*)
		ProportionalGain : REAL := 2.0; (*Gain factor [Asec]*)
		IntegralTime : REAL := 0.0; (*Integral time [s]*)
		FilterTime : REAL := 0.0; (*Filter time constant [s]*)
	END_STRUCT;
	MpAxisControllerFeedForwardType : 	STRUCT  (*Controller for feed forward*)
		TorqueLoad : REAL := 0.0; (*Load torque [Nm]*)
		TorquePositive : REAL := 0.0; (*Torque in positive direction [Nm]*)
		TorqueNegative : REAL := 0.0; (*Torque in negative direction [Nm]*)
		SpeedTorqueFactor : REAL := 0.0; (*Speed/torque factor [Nms]*)
		Inertia : REAL := 0.0; (*Mass moment of inertia [kgm�]*)
		AccelerationFilterTime : REAL := 0.0; (*Acceleration filter time constant [sec]*)
	END_STRUCT;
	MpAxisControllerVoltageFreqType : 	STRUCT  (*Controller for voltage frequency*)
		Type : MpAxisControllerUFTypeEnum := mpAXIS_CTRL_UF_LINEAR; (*Type of characteristic curve*)
		AutoConfig : MpAxisControllerUFAutoConfigEnum := mpAXIS_UF_CONFIG_OFF; (*Automatic configuration*)
		BoostVoltage : REAL; (*Gain voltage [V]*)
		RatedVoltage : REAL; (*Rated voltage [V]*)
		RatedFrequency : REAL; (*Rated frequency [cps]*)
		SlipCompensation : REAL; (*Slip compensation factor*)
	END_STRUCT;
	MpAxisControllerLoopFiltersType : 	STRUCT 
		FilterType : MpAxisControllerFilterTypeEnum := mpAXIS_LOOP_FILTER_NO_TRANSFERED; (*Filter type*)
		LowPass : MpAxisLoopFilterLowPassType; (*Low pass*)
		Notch : MpAxisLoopFilterNotchType; (*Notch*)
		ZTransferFunction : MpAxisLoopFilterZTransferFunType; (*Z Tranfer function*)
		Compensation : MpAxisLoopFilterCompensationType; (*Compensation*)
		Biquadratic : MpAxisLoopFilterBiquadraticType; (*Biquadratic*)
		Limiter : MpAxisLoopFilterLimiterType; (*Filter limiter*)
		LimiterLinear : MpAxisLoopFilterLimiterLinType; (*Filter limited*)
		LimiterRiseTime : MpAxisLoopFilterLimiterRTimeType; (*Limiter Rise Time*)
	END_STRUCT;
	MpAxisLoopFilterLowPassType : 	STRUCT 
		CutOffFrequency : REAL; (*Cut off frequency*)
	END_STRUCT;
	MpAxisLoopFilterNotchType : 	STRUCT 
		CenterFrequency : REAL; (*Center frequency*)
		Bandwidth : REAL; (*Bandwidth*)
	END_STRUCT;
	MpAxisLoopFilterZTransferFunType : 	STRUCT 
		A0 : REAL; (*Coefficient a0 of denominator polynomial*)
		A1 : REAL; (*Coefficient a1 of denominator polynomial*)
		B0 : REAL; (*Coefficient b0 of numerator polynominal*)
		B1 : REAL; (*Coefficient b1 of numerator polynomial*)
		B2 : REAL; (*Coefficient b2 of numerator polynomial*)
	END_STRUCT;
	MpAxisLoopFilterCompensationType : 	STRUCT 
		MultiplicationFactorParID : UINT; (*Multiplication factor ParID*)
		AdditiveValueParID : UINT; (*Additive value ParID*)
	END_STRUCT;
	MpAxisLoopFilterBiquadraticType : 	STRUCT  (*Frequency numerator*)
		FrequencyNumerator : REAL; (*Frequency numerator*)
		DampingNumerator : REAL; (*Damping numerator*)
		FrequencyDenominator : REAL; (*Frequency denominator*)
		DampingDenominator : REAL; (*Damping denominator*)
	END_STRUCT;
	MpAxisLoopFilterLimiterType : 	STRUCT 
		PositiveLimit : REAL; (*Fixed value for the positive limit*)
		NegativeLimit : REAL; (*Fixed value for the negative limit*)
		PositiveLimitParID : UINT; (*ParID the positive limit value is used from*)
		NegativeLimitParID : UINT; (*ParID the negative limit value is used from*)
	END_STRUCT;
	MpAxisLoopFilterLimiterLinType : 	STRUCT 
		InputParID : UINT; (*ACOPOS ParID for the input*)
		InputLimit : REAL; (*Input limit value for full limitation*)
		Gradient : REAL; (*Gradient*)
	END_STRUCT;
	MpAxisLoopFilterLimiterRTimeType : 	STRUCT 
		RiseTime : REAL; (*Rise time*)
		NormalizedLimit : REAL; (*Normalized limit*)
	END_STRUCT;
	MpAxisStopReactionType : 	STRUCT  (*Stop configuration*)
		Quickstop : MpAxisQuickStopEnum; (*Deceleration ramp after a quick stop has been enabled*)
		DriveError : MpAxisErrorStopEnum; (*Deceleration ramp after an axis error occurs*)
	END_STRUCT;
	MpAxisDigitalIOType : 	STRUCT  (*Digital inputs configuration*)
		Level : MpAxisDigitalIOLevelType; (*Level configuration*)
		Quickstop : MpAxisInputSourceEnum := mpAXIS_INPUT_NONE; (*Defines which input works as quickstop*)
	END_STRUCT;
	MpAxisDigitalIOLevelType : 	STRUCT  (*Input level configuration*)
		HomingSwitch : MpAxisLevelIOEnum := mpAXIS_IO_ACTIVE_HI; (*Reference switch*)
		PositiveLimitSwitch : MpAxisLevelIOEnum := mpAXIS_IO_ACTIVE_HI; (*Positive limit switch*)
		NegativeLimitSwitch : MpAxisLevelIOEnum := mpAXIS_IO_ACTIVE_HI; (*Negative limit switch*)
		Trigger1 : MpAxisLevelIOEnum := mpAXIS_IO_ACTIVE_HI; (*Trigger 1*)
		Trigger2 : MpAxisLevelIOEnum := mpAXIS_IO_ACTIVE_HI; (*Trigger 2 *)
	END_STRUCT;
	MpAxisBaseTypeEnum : 
		( (*System type options list*)
		mpAXIS_LIMITED_LINEAR := 0, (*The system is linear with active software limit positions*)
		mpAXIS_LIMITED_ROTARY := 1, (*The system is rotary with active software limit positions*)
		mpAXIS_PERIODIC_LINEAR := 2, (*The system is linear without active software limit positions but with a defined period*)
		mpAXIS_PERIODIC_ROTARY := 3, (*The system is rotary without active software limit positions but with a defined period*)
		mpAXIS_LINEAR := 4, (*The system is linear without active software limit positions and without period*)
		mpAXIS_ROTARY := 5 (*The system is rotary without active software limit positions and without period*)
		);
	MpAxisMeasurementUnitEnum : 
		( (*Units type options list*)
		mpAXIS_UNIT_DEGREES := 0, (*Degrees as the unit of measurement*)
		mpAXIS_UNIT_MM := 1, (*Millimeters as the unit of measurement*)
		mpAXIS_UNIT_GENERIC := 2, (*User-defined unit of measurement*)
		mpAXIS_UNIT_GENERIC_NOT_SCALED := 3 (*User-defined unit of measurement. Changing the MeasurementResolution, the system doesn't perform automatic scaling of configuration*)
		);
	MpAxisVelocityLimitModeEnum : 
		( (*Velocity limit mode options list*)
		mpAXIS_VEL_MODE_OFF := 0, (*Velocity error monitoring switched off*)
		mpAXIS_VEL_MODE_STOP_INIT := 1, (*Velocity error monitoring configured to "PeakVelocity"*)
		mpAXIS_VEL_MODE_STOP_AUTO1 := 2, (*Velocity error monitoring calculated with algorithm 1 *)
		mpAXIS_VEL_MODE_STOP_AUTO2 := 3 (*Velocity error monitoring calculated with algorithm 2*)
		);
	MpAxisControllerModeEnum : 
		( (*Controller mode options list*)
		mpAXIS_CTRL_MODE_POSITION := 1, (*Closed-loop control configured for position controller*)
		mpAXIS_CTRL_MODE_POSITION_FF := 33, (*Closed-loop control configured for feed-forward*)
		mpAXIS_CTRL_MODE_UF := 7 (*Closed-loop control configured for frequency inverter*)
		);
	MpAxisControllerUFTypeEnum : 
		( (*Voltage frequency type of characteristic curve*)
		mpAXIS_CTRL_UF_LINEAR := 1, (*Linear*)
		mpAXIS_CTRL_UF_QUADRATIC := 2, (*Quadratic*)
		mpAXIS_CTRL_UF_LINEAR2 := 3 (*Linear type 2*)
		);
	MpAxisControllerUFAutoConfigEnum : 
		( (*Automatic configuration*)
		mpAXIS_UF_CONFIG_OFF := 0, (*Automatic configuration disabled*)
		mpAXIS_UF_CONFIG_MOT_PAR := 1 (*Automatic configuration using the motor parameters*)
		);
	MpAxisControllerFilterTypeEnum : 
		(
		mpAXIS_LOOP_FILTER_NO_TRANSFERED := 0,
		mpAXIS_LOOP_FILTER_OFF := 1,
		mpAXIS_LOOP_FILTER_LOW_PASS := 2,
		mpAXIS_LOOP_FILTER_NOTCH := 3,
		mpAXIS_LOOP_FILTER_ZTRANSFER_FUN := 4,
		mpAXIS_LOOP_FILTER_COMPENSATION := 5,
		mpAXIS_LOOP_FILTER_BIQUADRATIC := 6,
		mpAXIS_LOOP_FILTER_LIMITER := 7,
		mpAXIS_LOOP_FILTER_LIMITER_LIN := 8,
		mpAXIS_LOOP_FILTER_LIMITER_RTIME := 9
		);
	MpAxisLevelIOEnum : 
		( (*Digital inputs configuration options list*)
		mpAXIS_IO_ACTIVE_LO := 0, (*Input configured as active low*)
		mpAXIS_IO_ACTIVE_HI := 1, (*Input configured as active high*)
		mpAXIS_IO_ACTIVE_LO_EXTERNAL := 288, (*Input configured as active low, forced externally*)
		mpAXIS_IO_ACTIVE_HI_EXTERNAL := 289 (*Input configured as active high, forced externally*)
		);
	MpAxisQuickStopEnum : 
		( (*Quick stop options list*)
		mpAXIS_QUICK_STOP_DEC_LIMIT := 20, (*Quick stop with the deceleration value defined in the axis limits*)
		mpAXIS_QUICK_STOP_TORQUE_LIMIT := 30, (*Quick stop using the torque limit*)
		mpAXIS_QUICK_STOP_INDUCTION := 40 (*Induction quick stop*)
		);
	MpAxisErrorStopEnum : 
		( (*Error stop options list*)
		mpAXIS_ERROR_STOP_DEC_LIMIT := 20, (*Stops with the deceleration value defined in the axis limits*)
		mpAXIS_ERROR_STOP_INDUCTION := 40, (*Induction stop*)
		mpAXIS_ERROR_STOP_CONTROL_OFF := 50 (*Immediate cutoff of power supply (drive no longer has electric torque)*)
		);
	MpAxisMotorDirectionEnum : 
		( (*Motor direction options list*)
		mpAXIS_DIR_CLOCKWISE := 0, (*Motor rotates clockwise*)
		mpAXIS_DIR_COUNTERCLOCKWISE := 255 (*Motor rotates counterclockwise*)
		);
	MpAxisPosVelOutputTypeEnum : 
		( (*Motor direction options list*)
		mpAXIS_OUTPUT_SET := 0, (*The value provided by Position or Velocity output is the set profile generated*)
		mpAXIS_OUTPUT_ACTUAL := 1 (*The value provided by Position or Velocity output is the real motor feedback*)
		);
END_TYPE
