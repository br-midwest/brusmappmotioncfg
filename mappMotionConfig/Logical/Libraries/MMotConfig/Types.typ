
TYPE
	ConfigWriteState_enum : 
		(
		WRITE_WAIT,
		WRITE_COMPARE,
		WRITE_BASE_TYPE,
		WRITE_CONFIG,
		WRITE_INIT,
		WRITE_DONE,
		WRITE_ERROR
		);
	ConfigInitState_enum : 
		(
		INIT_WAIT,
		INIT_BUSY,
		INIT_BASE_TYPE,
		INIT_MOVE_LIM,
		INIT_MECH_ELM,
		INIT_ACP_CTRL,
		INIT_MOVE_ERR_LIM,
		INIT_JERK_FILTER,
		INIT_DONE,
		INIT_ERROR
		);
	MMGrpUpdate_intern_typ : 	STRUCT 
		UpdateLast : BOOL;
		Step : MmPathGenAxGrpConfigStep_enum;
		ErrorStep : MmPathGenAxGrpConfigStep_enum;
		JointInfoStorage : ARRAY[0..99]OF McAGPGPAJntAxJntAxType;
		AxGrpFeatStorage : ARRAY[0..99]OF McCfgReferenceType;
		AxGrpCfgStorage : McCfgAxGrpPathGenType;
		MC_BR_ProcessConfig_0 : MC_BR_ProcessConfig;
		MC_BR_ProcessParam_0 : MC_BR_ProcessParam;
		MC_GroupReadStatus_0 : MC_GroupReadStatus;
	END_STRUCT;
	MmPathGenAxGrpConfigStep_enum : 
		(
		MGC_WAIT,
		MGC_GET_AX,
		MGC_WAIT_GET_AX,
		MGC_SAVE_CFG,
		MGC_WAIT_SAVE_CFG,
		MGC_WAIT_POWER_OFF,
		MGC_ACTIVATE_PARAM,
		MGC_WAIT_PARAM,
		MGC_DONE,
		MGC_RESETTING,
		MGC_ERROR
		);
	MmGrpInit_intern_typ : 	STRUCT 
		InitLast : BOOL;
		Step : MmPathGenAxGrpInitStep_enum;
		ErrorStep : MmPathGenAxGrpInitStep_enum;
		MC_BR_ProcessConfig_0 : MC_BR_ProcessConfig;
	END_STRUCT;
	MmPathGenAxGrpInitStep_enum : 
		(
		MGCI_WAIT,
		MGCI_GET_CFG,
		MGCI_WAIT_GET_CFG,
		MGCI_DONE,
		MGCI_ERROR,
		MGCI_RESETTING
		);
END_TYPE
