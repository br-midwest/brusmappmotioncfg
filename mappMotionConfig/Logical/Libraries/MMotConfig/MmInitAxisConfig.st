

FUNCTION_BLOCK MmInitAxisConfig
	
	IF Execute THEN
		CASE ConfigInitState OF
	
			INIT_WAIT:
				ConfigInitState := INIT_BUSY;
			
			INIT_BUSY:
				//Base Type
				InitConfig[0].DataType	:= mcCFG_AX_BASE_TYP;
				InitConfig[0].Execute	:= IncludeBaseType;
				// Movement Limits
				InitConfig[1].DataType	:= mcCFG_AX_MOVE_LIM;
				InitConfig[1].Execute	:= TRUE;
				// Mechanical Elements
				InitConfig[2].DataType	:= mcCFG_ACP_MECH_ELM;
				InitConfig[2].Execute	:= TRUE;
				// Controller
				InitConfig[3].DataType	:= mcCFG_ACP_CTRL;
				InitConfig[3].Execute	:= TRUE;
				// Movement Limits
				InitConfig[4].DataType	:= mcCFG_ACP_MOVE_ERR_LIM;
				InitConfig[4].Execute	:= TRUE;
				// Jerk Filter
				InitConfig[5].DataType	:= mcCFG_ACP_JERK_FLTR;
				InitConfig[5].Execute	:= TRUE;
				// Move to Done state
				IF Done THEN
					ConfigInitState := INIT_DONE;
				END_IF
				
			INIT_DONE:
				// Wait for Execute to be reset
			
			INIT_ERROR:
			// Wait for Execute to be reset
		
		END_CASE
	ELSE
		Done := FALSE;
		Error := FALSE;
		ConfigInitState := INIT_WAIT;
	END_IF
	
	Done := TRUE; // Set Flag
	// Call each FUB
	FOR i := 0 TO SIZEOF(InitConfig)/SIZEOF(InitConfig[0])-1 DO
		InitConfig[i].Component	:= ADR(AxisADR);
		InitConfig[i].Mode		:= mcPPM_LOAD_FROM_CONFIG;
		// Disable FBs if execute is false
		IF NOT Execute THEN
			InitConfig[i].Execute := FALSE;
		END_IF
		InitConfig[i]();
		// Report error if any FBs have error
		IF InitConfig[i].Error THEN
			Error := TRUE;
		END_IF
		// Report if FBs are not done, ignore index 0 if we are skipping base type
		IF NOT InitConfig[i].Done AND NOT (i = 0 AND NOT IncludeBaseType)THEN
			Done := FALSE;
		END_IF
	END_FOR
	
	// Move to INIT_ERROR
	IF Error THEN
		ConfigInitState := INIT_ERROR;
	END_IF
	
	// Busy status
	Busy := ConfigInitState = INIT_BUSY;
	
END_FUNCTION_BLOCK
