
(* TODO: Add your comment here *)
FUNCTION_BLOCK MmGroupSettingsInit
    
    // it may seem pointlesss to wrap ProcessConfig inside a FUB, but the advantage for me is the variable 
    // structure is pre-defined and connected without having to go look up and declare the requried types.
    
    IF NOT Init AND Intern.InitLast THEN
        Intern.Step := MGCI_RESETTING;
    END_IF;
    
    CASE Intern.Step OF
        MGCI_WAIT:
            IF Init THEN
                Busy := TRUE;
                Intern.Step := MGCI_GET_CFG;
            END_IF;
            		  
        MGCI_GET_CFG:
    
            Intern.MC_BR_ProcessConfig_0.Execute := TRUE;
            Intern.MC_BR_ProcessConfig_0.Name := GroupName;
            Intern.MC_BR_ProcessConfig_0.DataType := mcCFG_AXGRP_PATHGEN; 
            Intern.MC_BR_ProcessConfig_0.DataAddress := ADR(GroupSettings); 
            Intern.MC_BR_ProcessConfig_0.Mode := mcPCM_LOAD;
        
            Intern.Step := MGCI_WAIT_GET_CFG;
            
        MGCI_WAIT_GET_CFG:
            IF Intern.MC_BR_ProcessConfig_0.Error THEN
                Intern.ErrorStep := Intern.Step;
                Intern.Step := MGCI_ERROR;
            ELSIF Intern.MC_BR_ProcessConfig_0.Done THEN
                Intern.MC_BR_ProcessConfig_0.Execute := FALSE;
                Intern.Step := MGCI_DONE;
            END_IF;
        
        MGCI_DONE:    
            Done := TRUE;
            Busy := FALSE;
        
        MGCI_ERROR:
            Error := TRUE;
            Busy := FALSE;
        
        MGCI_RESETTING:
            Error := FALSE;
            Done := FALSE;
            Busy := FALSE;
            Intern.MC_BR_ProcessConfig_0.Execute := FALSE;
            Intern.Step := MGCI_WAIT;
      
    END_CASE;
    
    Intern.InitLast := Init;
    Intern.MC_BR_ProcessConfig_0();
    
END_FUNCTION_BLOCK
