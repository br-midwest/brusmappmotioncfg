
(*********************************************************************************
* Copyright: B&R Industrial Automation GmbH 
* Author:    olsonb 
* Created:   January 19, 2022/2:47 PM 
* 
* A library to convert the MpAxisBasicConfig structure into mappMotion configuration
* structures and assign them to the drive using MC_BR_ProcessConfig. It also initializes
* the changes made with MC_BR_ProcessConfig with MC_BR_ProcessParam mode
* mcPPM_LOAD_FROM_CONFIG. Reading the current configuration is done with MC_BR_ProcessConfig.
* This library modifies parameters contained in the MpAxisBasicConfig structure, and any
* additional parameters will default back to what is contained in the project configuration.
* 
* See WriteMappMotionConfig and ReadMappMotionConfig source code for more details.
* 
* The following cases require a restart of the PLC (indicated by RestartRequired)
* 		Changing Base Type (Linear vs rotary, periodic vs nonperiodic, bounded vs unbounded)
*		Changing Measurement Units or Resolution
*		Changing the Period (when Base Type is periodic) or Gearbox Direction
* 		Changing the axis reference that is mapped to a hardware module
* Limitations of this library:
* 		Loop Filters are not supported because they contain an absurd amount of parameters (>200)
* 		Digital Inputs are not supported because the BasicConfig parameters are not compatible
*		Loop Filters and Digital Inputs can be defined in the Physical View configuration if desired
*		Measurement Units are only supported if using MM or DEGREES, and only for the appropriate axis type
*		If the axis type and unit are incompatible, the configuraiton will default to the mappMotion Global Settings
*********************************************************************************)

FUNCTION_BLOCK MmAxisConfig

	IF Enable THEN
		
		// WriteConfig Inputs
		WriteConfig.AxisADR			:= ADR(AxisADR);
		WriteConfig.AxisLocation	:= AxisLocation;
		WriteConfig.BasicConfig		:= ADR(BasicConfig);
		WriteConfig.Execute			:= Save;
		// ReadConfig Inputs
		ReadConfig.AxisLocation		:= AxisLocation;
		ReadConfig.AxisADR			:= ADR(AxisADR);
		ReadConfig.Execute			:= Load;
		
		// Call FUBs
		WriteConfig();
		ReadConfig();
		
		// Outputs
		Busy	:= WriteConfig.Busy OR ReadConfig.Busy;
		Error	:= WriteConfig.Error OR ReadConfig.Error;
		IF Save AND WriteConfig.Done THEN
			Done := TRUE;
		ELSIF Load AND ReadConfig.Done THEN
			IF NOT Done THEN
				BasicConfig := ReadConfig.BasicConfig;	// Set BasicConfig only once
				Done := TRUE;
			END_IF
		ELSE
			Done := FALSE;
		END_IF
		RestartRequired := WriteConfig.RestartRequired;
		
    ELSE
        
        // Call FUBs
        WriteConfig(Execute := FALSE);
        ReadConfig(Execute := FALSE);
        
        // Outputs
        Busy        	:= FALSE;
        Error       	:= FALSE;
        Done            := FALSE;
        
	END_IF

END_FUNCTION_BLOCK