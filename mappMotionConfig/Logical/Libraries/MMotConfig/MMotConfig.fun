(*Primary Mapp Motion Configuration Function Blocks*)

FUNCTION_BLOCK MmAxisConfig (*(MM v5.13+) Replaces MpAxisBasicConfig FUB, can read and write mappMotion configurations. May require a warm restart for changes to take effect.*)
	VAR_INPUT
		AxisADR : REFERENCE TO McAxisType; (*ADR of the axis to be configured, must be valid axis defined in the Configuration View and match AxisName. Used in MC_BR_ProcessParam*) (* *) (*#PAR#*)
		AxisLocation : STRING[250]; (*Hardware location of the axis in the format of "Interface"/DriveConfiguration/Channel"Channel number", used in MC_BR_ProcessConfig. Ex: 'SL2.IF1.ST2/DriveConfiguration/Channel1'*) (* *) (*#PAR#*)
		BasicConfig : REFERENCE TO MpAxisBasicConfigType; (*ADR of MpAxisBasicConfigType to read/write*) (* *) (*#CMD#*)
		Enable : BOOL; (* *) (* *) (*#PAR#*)
		Load : BOOL; (* *) (* *) (*#CMD#*)
		Save : BOOL; (* *) (* *) (*#CMD#*)
	END_VAR
	VAR_OUTPUT
		Busy : BOOL; (* *) (* *) (*#CMD#*)
		Done : BOOL; (*Last command has finished. A restart may still be required to implement changes.*) (* *) (*#CMD#*)
		Error : BOOL; (*See logger for additional details. Reset by resetting Execute.*) (* *) (*#PAR#*)
		RestartRequired : BOOL; (*Some changes have not taken effect yet. A restart is required to implement them.*) (* *) (*#CMD#*)
	END_VAR
	VAR
		WriteConfig : MmWriteAxisConfig;
		ReadConfig : MmReadAxisConfig;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK MmGroupConfig (*(MM v5.11+) Path generated axis group configuration management FUB - Update will write any changes into the config and load them into the parameters to activate without reboot.*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Update : BOOL; (*Update parameters on GroupSettings into axis group config and parram systems*)
		GroupName : STRING[80]; (*Mapp Motion Axis Group Name *)
		GroupRef : {REDUND_UNREPLICABLE} McAxesGroupType; (*Axis group handle type McAxesGroupType*)
		GroupSettings : REFERENCE TO McCfgAxGrpPathGenType; (*ADR of the axis group settings structure*)
	END_VAR
	VAR_OUTPUT
		Busy : BOOL; (*Update is busy / in progress*) (* *) (*#CMD#*)
		Done : BOOL; (*Update completed successfully*) (* *) (*#CMD#*)
		Error : BOOL; (*See logger for additional details. Reset by resetting Update.*) (* *) (*#PAR#*)
		WaitingGroupPwrOff : BOOL; (*Power off is necessary to load from config into param.  Power off to continue when this flag is set.*)
	END_VAR
	VAR
		Intern : MMGrpUpdate_intern_typ; (*Internal Variables*)
	END_VAR
END_FUNCTION_BLOCK
(*Internal Helper FUBs*)

FUNCTION_BLOCK MmWriteAxisConfig (*Converts MpAxisBasicConfigType structure into structures used for mappMotion and writes them to a drive. May require a warm restart for changes to take effect.*)
	VAR_INPUT
		AxisADR : REFERENCE TO McAxisType; (*ADR of the axis to be configured, must be valid axis defined in the Configuration View and match AxisName. Used in MC_BR_ProcessParam*) (* *) (*#PAR#*)
		AxisLocation : STRING[250]; (*Hardware location of the axis in the format of "Interface"/DriveConfiguration/Channel"Channel number", used in MC_BR_ProcessConfig. Ex: 'SL2.IF1.ST2/DriveConfiguration/Channel1'*) (* *) (*#PAR#*)
		Execute : BOOL; (* *) (* *) (*#CMD#*)
		BasicConfig : REFERENCE TO MpAxisBasicConfigType; (*ADR of MpAxisBasicConfigType to read/write*) (* *) (*#CMD#*)
	END_VAR
	VAR_OUTPUT
		Busy : BOOL; (* *) (* *) (*#CMD#*)
		Done : BOOL; (*All parameters have been succesfully written, but a restart may still be required to implement them.*) (* *) (*#CMD#*)
		Error : BOOL; (*See logger for additional details. Reset by resetting Execute. Reset by resetting Execute.*) (* *) (*#PAR#*)
		RestartRequired : BOOL; (*Some changes have not taken effect yet. A restart is required to implement them.*) (* *) (*#CMD#*)
	END_VAR
	VAR
		ReadConfig : MmReadAxisConfig; (*Custom FUB to execute MC_BR_ProcessConfig in mcPCM_LOAD mode*)
		WriteConfig : ARRAY[0..1] OF MC_BR_ProcessConfig;
		InitConfig : MmInitAxisConfig; (*Custom FUB to execute MC_BR_ProcessParam in mcPPM_LOAD_FROM_CONFIG mode*)
		ConfigWriteState : ConfigWriteState_enum; (*State variable*)
		SoftwareConfigWrite : McCfgAxType; (*mappMotion configuration structure, synced with BasicConfig in action*)
		HardwareConfigWrite : McCfgAcpAxType; (*mappMotion configuration structure, synced with BasicConfig in action*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK MmReadAxisConfig (*Reads the current mappMotion configration from the drive. Reports as Hardware, Software, and Basic configurations.*)
	VAR_INPUT
		AxisADR : REFERENCE TO McAxisType;
		AxisLocation : STRING[250]; (*Hardware location of the axis in the format of "Interface"/DriveConfiguration/Channel"Channel number", used in MC_BR_ProcessConfig. Ex: 'SL2.IF1.ST2/DriveConfiguration/Channel1'*) (* *) (*#PAR#*)
		Execute : BOOL; (* *) (* *) (*#CMD#*)
	END_VAR
	VAR_OUTPUT
		Busy : BOOL; (* *) (* *) (*#CMD#*)
		Done : BOOL; (* *) (* *) (*#CMD#*)
		Error : BOOL; (*See logger for additional details. Reset by resetting Execute.*) (* *) (*#PAR#*)
		BasicConfig : MpAxisBasicConfigType; (*mappMotion parameters in an MpAxisBasicConfigType structure*) (* *) (*#CMD#*)
		SoftwareConfigRead : McCfgAxType; (*mappMotion configuration structure*) (* *) (*#CMD#OPT#*)
		HardwareConfigRead : McCfgAcpAxType; (*mappMotion configuration structure*) (* *) (*#CMD#OPT#*)
	END_VAR
	VAR
		ReadHardwareConfig : MC_BR_ProcessConfig; (*Reads HardwareConfig with MC_BR_ProcessConfig*)
		ReadSoftwareConfig : ARRAY[0..1] OF MC_BR_ProcessParam; (*Reads SoftwareConfig with MC_BR_ProcessParam*)
		BaseType : McCfgAxBaseTypType; (*mappMotion configuration structure, used in MC_BR_ProcessParam*)
		MovementLimits : McCfgAxMoveLimType; (*mappMotion configuration structure, used in MC_BR_ProcessParam*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK MmInitAxisConfig (*Initializes all parameters in the basic configuration, can replace a warm restart when using MC_BR_ProcessConfig. Executes MC_BR_ProcessParam with mode mcPPM_LOAD_FROM_CONFIG.*)
	VAR_INPUT
		AxisADR : REFERENCE TO McAxisType; (*ADR of the axis to be configured, must be valid axis defined in the Configuration View. Used in MC_BR_ProcessParam*) (* *) (*#PAR#*)
		Execute : BOOL; (* *) (* *) (*#CMD#*)
		IncludeBaseType : BOOL; (*BaseType should only be initialized if it has changed.*) (* *) (*#CMD#*)
	END_VAR
	VAR_OUTPUT
		Busy : BOOL; (* *) (* *) (*#CMD#*)
		Done : BOOL; (* *) (* *) (*#CMD#*)
		Error : BOOL; (*See logger for additional details. Reset by resetting Execute.*) (* *) (*#PAR#*)
	END_VAR
	VAR
		InitConfig : ARRAY[0..5] OF MC_BR_ProcessParam;
		ConfigInitState : ConfigInitState_enum;
		i : INT;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK MmGroupSettingsInit (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Init : BOOL;
		GroupName : STRING[80];
		GroupRef : McAxesGroupType;
		GroupSettings : REFERENCE TO McCfgAxGrpPathGenType;
	END_VAR
	VAR_OUTPUT
		Busy : BOOL;
		Done : BOOL;
		Error : BOOL;
	END_VAR
	VAR
		Intern : MmGrpInit_intern_typ;
	END_VAR
END_FUNCTION_BLOCK
