
ACTION BasicConfigToMappConfig: 
	
	(***** Convert BasicConfig to SoftwareConfigWrite and HardwareConfigWrite *****)
	(* Axis Name *)
	HardwareConfigWrite.AxisReference.Name := BasicConfig.AxisName;
	
	(* Measurement Units - Default to global settings *)
	SoftwareConfigWrite.BaseType.LinearBounded.MeasurementUnit	:= mcCLLU_G_SET;
	SoftwareConfigWrite.BaseType.RotaryBounded.MeasurementUnit	:= mcCLRU_G_SET;
	SoftwareConfigWrite.BaseType.LinearPeriodic.MeasurementUnit	:= mcCLLU_G_SET;
	SoftwareConfigWrite.BaseType.RotaryPeriodic.MeasurementUnit	:= mcCLRU_G_SET;
	SoftwareConfigWrite.BaseType.Linear.MeasurementUnit			:= mcCLLU_G_SET;
	SoftwareConfigWrite.BaseType.Rotary.MeasurementUnit			:= mcCLRU_G_SET;
	
	(* Measurement Resolution - Default to 0*)
	SoftwareConfigWrite.BaseType.LinearBounded.MeasurementResolution	:= 0;
	SoftwareConfigWrite.BaseType.RotaryBounded.MeasurementResolution	:= 0;
	SoftwareConfigWrite.BaseType.LinearPeriodic.MeasurementResolution	:= 0;
	SoftwareConfigWrite.BaseType.RotaryPeriodic.MeasurementResolution	:= 0;
	SoftwareConfigWrite.BaseType.Linear.MeasurementResolution			:= 0;
	SoftwareConfigWrite.BaseType.Rotary.MeasurementResolution			:= 0;
	
	(* Period Settings - Default to 0*)
	SoftwareConfigWrite.BaseType.LinearPeriodic.PeriodSettings.Period	:= 0;
	SoftwareConfigWrite.BaseType.RotaryPeriodic.PeriodSettings.Period	:= 0;
	
	(* Gearbox - Default to Standard Direction*)
	HardwareConfigWrite.MechanicalElements.Gearbox.Input		:= BasicConfig.Drive.Gearbox.Input;
	HardwareConfigWrite.MechanicalElements.Gearbox.Output		:= BasicConfig.Drive.Gearbox.Output;
	SoftwareConfigWrite.BaseType.LinearBounded.CountDirection	:= mcCCD_STD;
	SoftwareConfigWrite.BaseType.RotaryBounded.CountDirection	:= mcCCD_STD;
	SoftwareConfigWrite.BaseType.LinearPeriodic.CountDirection	:= mcCCD_STD;
	SoftwareConfigWrite.BaseType.RotaryPeriodic.CountDirection	:= mcCCD_STD;
	SoftwareConfigWrite.BaseType.Linear.CountDirection			:= mcCCD_STD;
	SoftwareConfigWrite.BaseType.Rotary.CountDirection			:= mcCCD_STD;
	
	(* Base Type - Including Measurement Units/Resolution, Period, and Gearbox Direction*)
	CASE BasicConfig.Axis.BaseType OF
		mpAXIS_LIMITED_LINEAR:
			SoftwareConfigWrite.BaseType.Type := mcABT_LIN_BD;
			SoftwareConfigWrite.BaseType.LinearBounded.MeasurementResolution	:= BasicConfig.Axis.MeasurementResolution;
			SoftwareConfigWrite.BaseType.LinearBounded.CountDirection			:= BOOL_TO_INT(BasicConfig.Drive.Gearbox.Direction=255);
			IF BasicConfig.Axis.MeasurementUnit = mpAXIS_UNIT_MM THEN
				SoftwareConfigWrite.BaseType.LinearBounded.MeasurementUnit := mcCLLU_MILL;
			END_IF
		mpAXIS_LIMITED_ROTARY:
			SoftwareConfigWrite.BaseType.Type := mcABT_ROT_BD;
			SoftwareConfigWrite.BaseType.RotaryBounded.MeasurementResolution	:= BasicConfig.Axis.MeasurementResolution;
			SoftwareConfigWrite.BaseType.RotaryBounded.CountDirection			:= BOOL_TO_INT(BasicConfig.Drive.Gearbox.Direction=255);
			IF BasicConfig.Axis.MeasurementUnit = mpAXIS_UNIT_DEGREES THEN
				SoftwareConfigWrite.BaseType.RotaryBounded.MeasurementUnit := mcCLRU_DEG;
			END_IF
		mpAXIS_PERIODIC_LINEAR:
			SoftwareConfigWrite.BaseType.Type := mcABT_LIN_PER;
			SoftwareConfigWrite.BaseType.LinearPeriodic.MeasurementResolution	:= BasicConfig.Axis.MeasurementResolution;
			SoftwareConfigWrite.BaseType.LinearPeriodic.CountDirection			:= BOOL_TO_INT(BasicConfig.Drive.Gearbox.Direction=255);
			SoftwareConfigWrite.BaseType.LinearPeriodic.PeriodSettings.Period	:= BasicConfig.Axis.PeriodSettings.Period;
			IF BasicConfig.Axis.MeasurementUnit = mpAXIS_UNIT_MM THEN
				SoftwareConfigWrite.BaseType.LinearPeriodic.MeasurementUnit := mcCLLU_MILL;
			END_IF
		mpAXIS_PERIODIC_ROTARY:
			SoftwareConfigWrite.BaseType.Type := mcABT_ROT_PER;
			SoftwareConfigWrite.BaseType.RotaryPeriodic.MeasurementResolution	:= BasicConfig.Axis.MeasurementResolution;
			SoftwareConfigWrite.BaseType.RotaryPeriodic.CountDirection			:= BOOL_TO_INT(BasicConfig.Drive.Gearbox.Direction=255);
			SoftwareConfigWrite.BaseType.RotaryPeriodic.PeriodSettings.Period	:= BasicConfig.Axis.PeriodSettings.Period;
			IF BasicConfig.Axis.MeasurementUnit = mpAXIS_UNIT_DEGREES THEN
				SoftwareConfigWrite.BaseType.RotaryPeriodic.MeasurementUnit := mcCLRU_DEG;
			END_IF
		mpAXIS_LINEAR:
			SoftwareConfigWrite.BaseType.Type := mcABT_LIN;
			IF BasicConfig.Axis.MeasurementUnit = mpAXIS_UNIT_MM THEN
			SoftwareConfigWrite.BaseType.Linear.MeasurementResolution	:= BasicConfig.Axis.MeasurementResolution;
			SoftwareConfigWrite.BaseType.Linear.CountDirection			:= BOOL_TO_INT(BasicConfig.Drive.Gearbox.Direction=255);
				SoftwareConfigWrite.BaseType.Linear.MeasurementUnit := mcCLLU_MILL;
			END_IF
		mpAXIS_ROTARY:
			SoftwareConfigWrite.BaseType.Type := mcABT_ROT;
			SoftwareConfigWrite.BaseType.Rotary.MeasurementResolution	:= BasicConfig.Axis.MeasurementResolution;
			SoftwareConfigWrite.BaseType.Rotary.CountDirection			:= BOOL_TO_INT(BasicConfig.Drive.Gearbox.Direction=255);
			IF BasicConfig.Axis.MeasurementUnit = mpAXIS_UNIT_DEGREES THEN
				SoftwareConfigWrite.BaseType.Rotary.MeasurementUnit :=  mcCLRU_DEG;
			END_IF
	END_CASE
	
	(* Software Positions Limit *)
	// "Internal" limts apply (not compatible with jerk time)
	SoftwareConfigWrite.MovementLimits.Internal.Position.LowerLimit	:= BasicConfig.Axis.SoftwareLimitPositions.LowerLimit;
	SoftwareConfigWrite.MovementLimits.Internal.Position.UpperLimit	:= BasicConfig.Axis.SoftwareLimitPositions.UpperLimit;
	
	(* Movement Limits *)
	// "Basic" accel/decel apply
	SoftwareConfigWrite.MovementLimits.Internal.Acceleration.Basic.Acceleration	:= BasicConfig.Axis.MovementLimits.Acceleration;
	SoftwareConfigWrite.MovementLimits.Internal.Deceleration.Basic.Deceleration	:= BasicConfig.Axis.MovementLimits.Deceleration;
	// "Advanced" velocity limits apply
	SoftwareConfigWrite.MovementLimits.Internal.Velocity.Type				:= mcAMLV_ADV;
	SoftwareConfigWrite.MovementLimits.Internal.Velocity.Advanced.Positive	:= BasicConfig.Axis.MovementLimits.VelocityPositive;
	SoftwareConfigWrite.MovementLimits.Internal.Velocity.Advanced.Negative	:= BasicConfig.Axis.MovementLimits.VelocityNegative;
	
	(* Jerk Filter *)
	IF BasicConfig.Axis.MovementLimits.JerkTime > 0 THEN
		HardwareConfigWrite.JerkFilter.Type			:= mcAJF_USE;
	ELSE
		HardwareConfigWrite.JerkFilter.Type			:= mcAJF_NOT_USE;
	END_IF
	HardwareConfigWrite.JerkFilter.Used.JerkTime	:= BasicConfig.Axis.MovementLimits.JerkTime;
	(* Stop Limits *)
	HardwareConfigWrite.MovementErrorLimits.PositionError										:= BasicConfig.Axis.MovementLimits.PositionErrorStopLimit;
	HardwareConfigWrite.MovementErrorLimits.VelocityErrorMonitoring.UserDefined.VelocityError	:= BasicConfig.Axis.MovementLimits.VelocityErrorStopLimit;
	CASE BasicConfig.Axis.MovementLimits.VelocityErrorStopLimitMode OF
		mpAXIS_VEL_MODE_OFF:
			HardwareConfigWrite.MovementErrorLimits.VelocityErrorMonitoring.Type	:= mcAMELVEM_NOT_USE;
		mpAXIS_VEL_MODE_STOP_INIT:
			HardwareConfigWrite.MovementErrorLimits.VelocityErrorMonitoring.Type	:= mcAMELVEM_USRDEF;
		mpAXIS_VEL_MODE_STOP_AUTO1:
			HardwareConfigWrite.MovementErrorLimits.VelocityErrorMonitoring.Type	:= mcAMELVEM_AUT_1;
		mpAXIS_VEL_MODE_STOP_AUTO2:
			HardwareConfigWrite.MovementErrorLimits.VelocityErrorMonitoring.Type	:= mcAMELVEM_AUT_2;
	END_CASE
		
	(* Transformation *)
	HardwareConfigWrite.MechanicalElements.RotaryToLinearTransformation.ReferenceDistance	:= BasicConfig.Drive.Transformation.ReferenceDistance;
	
	(* Controller Mode *)
	// "Position Controller Model Based" not supported
	CASE BasicConfig.Drive.Controller.Mode OF
		mpAXIS_CTRL_MODE_POSITION:
			HardwareConfigWrite.Controller.Mode.Type	:= mcACM_POS_CTRL;	
		mpAXIS_CTRL_MODE_POSITION_FF:
			HardwareConfigWrite.Controller.Mode.Type	:= mcACM_POS_CTRL_TORQ_FF;
		mpAXIS_CTRL_MODE_UF:
			HardwareConfigWrite.Controller.Mode.Type	:= mcACM_V_FREQ_CTRL;
	END_CASE
	
	(* Position Controller *)
	// Loop filters not supported - Configure in Hardware Configuration if desired
	HardwareConfigWrite.Controller.Mode.PositionController.Position.ProportionalGain	:= BasicConfig.Drive.Controller.Position.ProportionalGain;
	HardwareConfigWrite.Controller.Mode.PositionController.Position.IntegrationTime	:= BasicConfig.Drive.Controller.Position.IntegralTime;
	HardwareConfigWrite.Controller.Mode.PositionController.Position.PredictionTime	:= BasicConfig.Drive.Controller.Position.PredictionTime;
	HardwareConfigWrite.Controller.Mode.PositionController.Position.TotalDelayTime	:= BasicConfig.Drive.Controller.Position.TotalDelayTime;

	HardwareConfigWrite.Controller.Mode.PositionController.Speed.ProportionalGain	:= BasicConfig.Drive.Controller.Speed.ProportionalGain;
	HardwareConfigWrite.Controller.Mode.PositionController.Speed.IntegrationTime		:= BasicConfig.Drive.Controller.Speed.IntegralTime;
	HardwareConfigWrite.Controller.Mode.PositionController.Speed.FilterTime			:= BasicConfig.Drive.Controller.Speed.FilterTime;
	
	(* Feed Forward *)
	// Loop filters not supported - Configure in Hardware Configuration if desired
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.Position.ProportionalGain	:= BasicConfig.Drive.Controller.Position.ProportionalGain;
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.Position.IntegrationTime	:= BasicConfig.Drive.Controller.Position.IntegralTime;
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.Position.TotalDelayTime	:= BasicConfig.Drive.Controller.Position.TotalDelayTime;

	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.Speed.ProportionalGain	:= BasicConfig.Drive.Controller.Speed.ProportionalGain;
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.Speed.IntegrationTime		:= BasicConfig.Drive.Controller.Speed.IntegralTime;
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.Speed.FilterTime			:= BasicConfig.Drive.Controller.Speed.FilterTime;
	// "Standard" feed forward mode applies
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.TorqueLoad				:= BasicConfig.Drive.Controller.FeedForward.TorqueLoad;
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.TorquePositive			:= BasicConfig.Drive.Controller.FeedForward.TorquePositive;
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.TorqueNegative			:= BasicConfig.Drive.Controller.FeedForward.TorqueNegative;
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.SpeedTorqueFactor		:= BasicConfig.Drive.Controller.FeedForward.SpeedTorqueFactor;
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.Inertia					:= BasicConfig.Drive.Controller.FeedForward.Inertia;
	HardwareConfigWrite.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.AccelerationFilterTime	:= BasicConfig.Drive.Controller.FeedForward.AccelerationFilterTime;
	
	(* Voltage Frequency Control *)
	CASE BasicConfig.Drive.Controller.VoltageFrequency.Type OF
		mpAXIS_CTRL_UF_LINEAR:
			HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.Type	:= mcACMVFCVFT_LIN;
		mpAXIS_CTRL_UF_QUADRATIC:
			HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.Type	:= mcACMVFCVFT_CONST_LD_TORQ;
		mpAXIS_CTRL_UF_LINEAR2:
			HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.Type	:= mcACMVFCVFT_QUAD;
	END_CASE
	
	CASE BasicConfig.Drive.Controller.VoltageFrequency.AutoConfig OF
		mpAXIS_UF_CONFIG_OFF:
			HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.AutomaticConfiguration.Type	:=  mcACMVFCVFAC_NOT_USE;
		mpAXIS_UF_CONFIG_MOT_PAR:
			HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.AutomaticConfiguration.Type	:=  mcACMVFCVFAC_MOT_PAR_BASED;
	END_CASE
	HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.AutomaticConfiguration.NotUsed.BoostVoltage		:= BasicConfig.Drive.Controller.VoltageFrequency.BoostVoltage;
	HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.AutomaticConfiguration.NotUsed.RatedVoltage		:= BasicConfig.Drive.Controller.VoltageFrequency.RatedVoltage;
	HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.AutomaticConfiguration.NotUsed.RatedFrequency	:= BasicConfig.Drive.Controller.VoltageFrequency.RatedFrequency;
	HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.SlipCompensation	:= BasicConfig.Drive.Controller.VoltageFrequency.SlipCompensation;
	HardwareConfigWrite.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.TotalDelayTime		:= BasicConfig.Drive.Controller.Position.TotalDelayTime;
	
	(* Stop Reaction *)
	// "Deceleration Limit with Jerk Filter" not supported
	CASE BasicConfig.Drive.StopReaction.Quickstop OF
		mpAXIS_QUICK_STOP_DEC_LIMIT:
			HardwareConfigWrite.StopReaction.Quickstop.Type	:= mcASRQ_DEC_LIM;
		mpAXIS_QUICK_STOP_TORQUE_LIMIT:
			HardwareConfigWrite.StopReaction.Quickstop.Type	:= mcASRQ_TORQ_LIM;
		mpAXIS_QUICK_STOP_INDUCTION:
			HardwareConfigWrite.StopReaction.Quickstop.Type	:= mcASRQ_INDUCT_HALT;
	END_CASE
	(* Drive Error *)
	CASE BasicConfig.Drive.StopReaction.DriveError OF
		mpAXIS_ERROR_STOP_DEC_LIMIT:
			HardwareConfigWrite.StopReaction.DriveError.Type	:= mcASRDE_DEC_LIM;
		mpAXIS_ERROR_STOP_INDUCTION:
			HardwareConfigWrite.StopReaction.DriveError.Type	:= mcASRDE_INDUCT_HALT;
		mpAXIS_ERROR_STOP_CONTROL_OFF:
			HardwareConfigWrite.StopReaction.DriveError.Type	:= mcASRDE_COAST_TO_STANDSTILL;
	END_CASE
	(* Digital Inputs *)
	// Digital inputs not supported - Configure in Hardware Configuration if desired
	
END_ACTION

ACTION MappConfigToBasicConfig:
	
	(***** Convert BasicConfig to SoftwareConfigRead and HardwareConfigRead *****)
	(* Axis Name *)
	BasicConfig.AxisName	:= HardwareConfigRead.AxisReference.Name;
	
	(* Base Type, Measurement Resolution, Period Settings, Measurement Units (if compatible) *)
	BasicConfig.Axis.MeasurementUnit := mpAXIS_UNIT_GENERIC;	// Default to generic
	CASE SoftwareConfigRead.BaseType.Type OF
		mcABT_LIN_BD:
			BasicConfig.Axis.BaseType := mpAXIS_LIMITED_LINEAR;
			BasicConfig.Axis.MeasurementResolution	:= SoftwareConfigRead.BaseType.LinearBounded.MeasurementResolution;
			IF SoftwareConfigRead.BaseType.LinearBounded.MeasurementUnit = mcCLLU_MILL THEN
				BasicConfig.Axis.MeasurementUnit := mpAXIS_UNIT_MM;
			END_IF
		mcABT_ROT_BD:
			BasicConfig.Axis.BaseType := mpAXIS_LIMITED_ROTARY;
			BasicConfig.Axis.MeasurementResolution	:= SoftwareConfigRead.BaseType.RotaryBounded.MeasurementResolution;
			IF SoftwareConfigRead.BaseType.RotaryBounded.MeasurementUnit = mcCLRU_DEG THEN
				BasicConfig.Axis.MeasurementUnit := mpAXIS_UNIT_DEGREES;
			END_IF
		mcABT_LIN_PER:
			BasicConfig.Axis.BaseType := mpAXIS_PERIODIC_LINEAR;
			BasicConfig.Axis.MeasurementResolution	:= SoftwareConfigRead.BaseType.LinearPeriodic.MeasurementResolution;
			BasicConfig.Axis.PeriodSettings.Period	:= SoftwareConfigRead.BaseType.LinearPeriodic.PeriodSettings.Period;
			IF SoftwareConfigRead.BaseType.LinearPeriodic.MeasurementUnit = mcCLLU_MILL THEN
				BasicConfig.Axis.MeasurementUnit := mpAXIS_UNIT_MM;
			END_IF
		mcABT_ROT_PER:
			BasicConfig.Axis.BaseType := mpAXIS_PERIODIC_ROTARY;
			BasicConfig.Axis.MeasurementResolution	:= SoftwareConfigRead.BaseType.RotaryPeriodic.MeasurementResolution;
			BasicConfig.Axis.PeriodSettings.Period	:= SoftwareConfigRead.BaseType.RotaryPeriodic.PeriodSettings.Period;
			IF SoftwareConfigRead.BaseType.RotaryPeriodic.MeasurementUnit = mcCLRU_DEG THEN
				BasicConfig.Axis.MeasurementUnit := mpAXIS_UNIT_DEGREES;
			END_IF
		mcABT_LIN:
			BasicConfig.Axis.BaseType := mpAXIS_LINEAR;
			BasicConfig.Axis.MeasurementResolution	:= SoftwareConfigRead.BaseType.Linear.MeasurementResolution;
			IF SoftwareConfigRead.BaseType.Linear.MeasurementUnit = mcCLLU_MILL THEN
				BasicConfig.Axis.MeasurementUnit := mpAXIS_UNIT_MM;
			END_IF
		mcABT_ROT:
			BasicConfig.Axis.BaseType := mpAXIS_ROTARY;
			BasicConfig.Axis.MeasurementResolution	:= SoftwareConfigRead.BaseType.Rotary.MeasurementResolution;
			IF SoftwareConfigRead.BaseType.Rotary.MeasurementUnit =  mcCLRU_DEG THEN
				BasicConfig.Axis.MeasurementUnit := mpAXIS_UNIT_DEGREES;
			END_IF
	END_CASE
	
	(* Movement Limits *)
	// "External" type not supported
	CASE SoftwareConfigRead.MovementLimits.Type OF
		mcAML_INT:
			// Positoin
			BasicConfig.Axis.SoftwareLimitPositions.LowerLimit	:= SoftwareConfigRead.MovementLimits.Internal.Position.LowerLimit;
			BasicConfig.Axis.SoftwareLimitPositions.UpperLimit	:= SoftwareConfigRead.MovementLimits.Internal.Position.UpperLimit;
			// Velocity
			CASE SoftwareConfigRead.MovementLimits.Internal.Velocity.Type OF
				mcAMLV_BASIC:
					BasicConfig.Axis.MovementLimits.VelocityPositive	:= SoftwareConfigRead.MovementLimits.Internal.Velocity.Basic.Velocity;
					BasicConfig.Axis.MovementLimits.VelocityNegative	:= SoftwareConfigRead.MovementLimits.Internal.Velocity.Basic.Velocity;
				mcAMLV_ADV:
					BasicConfig.Axis.MovementLimits.VelocityPositive	:= SoftwareConfigRead.MovementLimits.Internal.Velocity.Advanced.Positive;
					BasicConfig.Axis.MovementLimits.VelocityNegative	:= SoftwareConfigRead.MovementLimits.Internal.Velocity.Advanced.Negative;
			END_CASE
			// Acceleration - Advanced configuration takes positive value
			CASE SoftwareConfigRead.MovementLimits.Internal.Acceleration.Type OF
				mcAMLA_BASIC:
					BasicConfig.Axis.MovementLimits.Acceleration		:= SoftwareConfigRead.MovementLimits.Internal.Acceleration.Basic.Acceleration;
				mcAMLA_ADV:
					BasicConfig.Axis.MovementLimits.Acceleration		:= SoftwareConfigRead.MovementLimits.Internal.Acceleration.Advanced.Positive;
			END_CASE
			// Deceleration - Advanced configuration takes positive value
			CASE SoftwareConfigRead.MovementLimits.Internal.Deceleration.Type OF
				mcAMLD_BASIC:
					BasicConfig.Axis.MovementLimits.Deceleration		:= SoftwareConfigRead.MovementLimits.Internal.Deceleration.Basic.Deceleration;
				mcAMLD_ADV:
					BasicConfig.Axis.MovementLimits.Deceleration		:= SoftwareConfigRead.MovementLimits.Internal.Deceleration.Advanced.Positive;
			END_CASE
			
		mcAML_INT_PATH_AX:
			// Positoin
			BasicConfig.Axis.SoftwareLimitPositions.LowerLimit	:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Position.LowerLimit;
			BasicConfig.Axis.SoftwareLimitPositions.UpperLimit	:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Position.UpperLimit;
			// Velocity
			CASE SoftwareConfigRead.MovementLimits.InternalPathAxis.Velocity.Type OF
				mcAMLV_BASIC:
					BasicConfig.Axis.MovementLimits.VelocityPositive	:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Velocity.Basic.Velocity;
					BasicConfig.Axis.MovementLimits.VelocityNegative	:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Velocity.Basic.Velocity;
				mcAMLV_ADV:
					BasicConfig.Axis.MovementLimits.VelocityPositive	:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Velocity.Advanced.Positive;
					BasicConfig.Axis.MovementLimits.VelocityNegative	:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Velocity.Advanced.Negative;
			END_CASE
			// Acceleration - Advanced configuration takes positive value
			CASE SoftwareConfigRead.MovementLimits.InternalPathAxis.Acceleration.Type OF
				mcAMLA_BASIC:
					BasicConfig.Axis.MovementLimits.Acceleration		:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Acceleration.Basic.Acceleration;
				mcAMLA_ADV:
					BasicConfig.Axis.MovementLimits.Acceleration		:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Acceleration.Advanced.Positive;
			END_CASE
			// Deceleration - Advanced configuration takes positive value
			CASE SoftwareConfigRead.MovementLimits.InternalPathAxis.Deceleration.Type OF
				mcAMLD_BASIC:
					BasicConfig.Axis.MovementLimits.Deceleration		:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Deceleration.Basic.Deceleration;
				mcAMLD_ADV:
					BasicConfig.Axis.MovementLimits.Deceleration		:= SoftwareConfigRead.MovementLimits.InternalPathAxis.Deceleration.Advanced.Positive;
			END_CASE
	END_CASE
	
	(* Jerk Filter *)
	CASE HardwareConfigRead.JerkFilter.Type OF
		mcAJF_USE:
			BasicConfig.Axis.MovementLimits.JerkTime	:= HardwareConfigRead.JerkFilter.Used.JerkTime;
		mcAJF_NOT_USE:
			BasicConfig.Axis.MovementLimits.JerkTime	:= 0;
	END_CASE

	(* Stop Limits *)
	BasicConfig.Axis.MovementLimits.PositionErrorStopLimit	:= HardwareConfigRead.MovementErrorLimits.PositionError;
	CASE HardwareConfigRead.MovementErrorLimits.VelocityErrorMonitoring.Type OF
		mcAMELVEM_AUT_1:
			BasicConfig.Axis.MovementLimits.VelocityErrorStopLimitMode := mpAXIS_VEL_MODE_STOP_AUTO1;
		mcAMELVEM_AUT_2:
			BasicConfig.Axis.MovementLimits.VelocityErrorStopLimitMode := mpAXIS_VEL_MODE_STOP_AUTO2;
		mcAMELVEM_USRDEF:
			BasicConfig.Axis.MovementLimits.VelocityErrorStopLimitMode := mpAXIS_VEL_MODE_STOP_INIT;
			BasicConfig.Axis.MovementLimits.VelocityErrorStopLimit	:= HardwareConfigRead.MovementErrorLimits.VelocityErrorMonitoring.UserDefined.VelocityError;
		mcAMELVEM_NOT_USE:
			BasicConfig.Axis.MovementLimits.VelocityErrorStopLimitMode := mpAXIS_VEL_MODE_OFF;
	END_CASE

	(* Gearbox *)
	BasicConfig.Drive.Gearbox.Input		:= HardwareConfigRead.MechanicalElements.Gearbox.Input;
	BasicConfig.Drive.Gearbox.Output	:= HardwareConfigRead.MechanicalElements.Gearbox.Output;
	CASE SoftwareConfigRead.BaseType.Type OF
		mcABT_LIN_BD:
			BasicConfig.Axis.BaseType := mpAXIS_LIMITED_LINEAR;
			CASE SoftwareConfigRead.BaseType.LinearBounded.CountDirection OF
				mcCCD_STD:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_CLOCKWISE;
				mcCCD_INV:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_COUNTERCLOCKWISE;
			END_CASE
		mcABT_ROT_BD:
			BasicConfig.Axis.BaseType := mpAXIS_LIMITED_ROTARY;
			CASE SoftwareConfigRead.BaseType.RotaryBounded.CountDirection OF
				mcCCD_STD:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_CLOCKWISE;
				mcCCD_INV:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_COUNTERCLOCKWISE;
			END_CASE
		mcABT_LIN_PER:
			BasicConfig.Axis.BaseType := mpAXIS_PERIODIC_LINEAR;
			CASE SoftwareConfigRead.BaseType.LinearPeriodic.CountDirection OF
				mcCCD_STD:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_CLOCKWISE;
				mcCCD_INV:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_COUNTERCLOCKWISE;
			END_CASE
		mcABT_ROT_PER:
			BasicConfig.Axis.BaseType := mpAXIS_PERIODIC_ROTARY;
			CASE SoftwareConfigRead.BaseType.RotaryPeriodic.CountDirection OF
				mcCCD_STD:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_CLOCKWISE;
				mcCCD_INV:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_COUNTERCLOCKWISE;
			END_CASE
		mcABT_LIN:
			BasicConfig.Axis.BaseType := mpAXIS_LINEAR;
			CASE SoftwareConfigRead.BaseType.Linear.CountDirection OF
				mcCCD_STD:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_CLOCKWISE;
				mcCCD_INV:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_COUNTERCLOCKWISE;
			END_CASE
		mcABT_ROT:
			BasicConfig.Axis.BaseType := mpAXIS_ROTARY;
			CASE SoftwareConfigRead.BaseType.Rotary.CountDirection OF
				mcCCD_STD:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_CLOCKWISE;
				mcCCD_INV:
					BasicConfig.Drive.Gearbox.Direction	:= mpAXIS_DIR_COUNTERCLOCKWISE;
			END_CASE
	END_CASE
		
	(* Transformation *)
	BasicConfig.Drive.Transformation.ReferenceDistance	:= HardwareConfigRead.MechanicalElements.RotaryToLinearTransformation.ReferenceDistance;
	
	(* Controller Mode *)
	// "Position Controller Model Based" not supported
	// Loop filters not supported
	CASE HardwareConfigRead.Controller.Mode.Type OF
		mcACM_POS_CTRL:
			BasicConfig.Drive.Controller.Mode	:= mpAXIS_CTRL_MODE_POSITION;
			(* Position Controller *)
			BasicConfig.Drive.Controller.Position.ProportionalGain	:= HardwareConfigRead.Controller.Mode.PositionController.Position.ProportionalGain;
			BasicConfig.Drive.Controller.Position.IntegralTime		:= HardwareConfigRead.Controller.Mode.PositionController.Position.IntegrationTime;
			BasicConfig.Drive.Controller.Position.PredictionTime	:= HardwareConfigRead.Controller.Mode.PositionController.Position.PredictionTime;
			BasicConfig.Drive.Controller.Position.TotalDelayTime	:= HardwareConfigRead.Controller.Mode.PositionController.Position.TotalDelayTime;

			BasicConfig.Drive.Controller.Speed.ProportionalGain		:= HardwareConfigRead.Controller.Mode.PositionController.Speed.ProportionalGain;
			BasicConfig.Drive.Controller.Speed.IntegralTime			:= HardwareConfigRead.Controller.Mode.PositionController.Speed.IntegrationTime;
			BasicConfig.Drive.Controller.Speed.FilterTime			:= HardwareConfigRead.Controller.Mode.PositionController.Speed.FilterTime;
			
		mcACM_POS_CTRL_TORQ_FF:
			BasicConfig.Drive.Controller.Mode	:= mpAXIS_CTRL_MODE_POSITION_FF;
			(* Feed Forward *)
			BasicConfig.Drive.Controller.Position.ProportionalGain	:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.Position.ProportionalGain;
			BasicConfig.Drive.Controller.Position.IntegralTime		:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.Position.IntegrationTime;
			BasicConfig.Drive.Controller.Position.TotalDelayTime	:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.Position.TotalDelayTime;

			BasicConfig.Drive.Controller.Speed.ProportionalGain	:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.Speed.ProportionalGain;
			BasicConfig.Drive.Controller.Speed.IntegralTime		:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.Speed.IntegrationTime;
			BasicConfig.Drive.Controller.Speed.FilterTime		:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.Speed.FilterTime;
			// "Standard" feed forward mode applies
			BasicConfig.Drive.Controller.FeedForward.TorqueLoad				:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.TorqueLoad;
			BasicConfig.Drive.Controller.FeedForward.TorquePositive			:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.TorquePositive;
			BasicConfig.Drive.Controller.FeedForward.TorqueNegative			:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.TorqueNegative;
			BasicConfig.Drive.Controller.FeedForward.SpeedTorqueFactor		:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.SpeedTorqueFactor;
			BasicConfig.Drive.Controller.FeedForward.Inertia				:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.Inertia;
			BasicConfig.Drive.Controller.FeedForward.AccelerationFilterTime	:= HardwareConfigRead.Controller.Mode.PositionControllerTorqueFf.FeedForward.Standard.AccelerationFilterTime;

		mcACM_V_FREQ_CTRL:
			BasicConfig.Drive.Controller.Mode	:= mpAXIS_CTRL_MODE_UF;
			(* Voltage Frequency Control *)
			CASE HardwareConfigRead.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.Type OF
				mcACMVFCVFT_LIN:
					BasicConfig.Drive.Controller.VoltageFrequency.Type	:= mpAXIS_CTRL_UF_LINEAR;
				mcACMVFCVFT_CONST_LD_TORQ:
					BasicConfig.Drive.Controller.VoltageFrequency.Type	:= mpAXIS_CTRL_UF_QUADRATIC;
				mcACMVFCVFT_QUAD:
					BasicConfig.Drive.Controller.VoltageFrequency.Type	:= mpAXIS_CTRL_UF_LINEAR2;
			END_CASE
	
			CASE HardwareConfigRead.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.AutomaticConfiguration.Type OF
				mcACMVFCVFAC_NOT_USE:
					BasicConfig.Drive.Controller.VoltageFrequency.AutoConfig 	:= mpAXIS_UF_CONFIG_OFF;
				mcACMVFCVFAC_MOT_PAR_BASED:
					BasicConfig.Drive.Controller.VoltageFrequency.AutoConfig 	:= mpAXIS_UF_CONFIG_MOT_PAR;
			END_CASE
			BasicConfig.Drive.Controller.VoltageFrequency.BoostVoltage		:= HardwareConfigRead.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.AutomaticConfiguration.NotUsed.BoostVoltage;
			BasicConfig.Drive.Controller.VoltageFrequency.RatedVoltage		:= HardwareConfigRead.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.AutomaticConfiguration.NotUsed.RatedVoltage;
			BasicConfig.Drive.Controller.VoltageFrequency.RatedFrequency	:= HardwareConfigRead.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.AutomaticConfiguration.NotUsed.RatedFrequency;
			BasicConfig.Drive.Controller.VoltageFrequency.SlipCompensation	:= HardwareConfigRead.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.SlipCompensation;
			BasicConfig.Drive.Controller.Position.TotalDelayTime			:= HardwareConfigRead.Controller.Mode.VoltageFrequencyControl.VoltageFrequency.TotalDelayTime;
	
	END_CASE
	
	(* Stop Reaction *)
	// "Deceleration Limit with Jerk Filter" not supported
	CASE HardwareConfigRead.StopReaction.Quickstop.Type OF
		mcASRQ_DEC_LIM:
			BasicConfig.Drive.StopReaction.Quickstop	:= mpAXIS_QUICK_STOP_DEC_LIMIT;
		mcASRQ_TORQ_LIM:
			BasicConfig.Drive.StopReaction.Quickstop	:= mpAXIS_QUICK_STOP_TORQUE_LIMIT;
		mcASRQ_INDUCT_HALT:
			BasicConfig.Drive.StopReaction.Quickstop	:= mpAXIS_QUICK_STOP_INDUCTION;
	END_CASE
	(* Drive Error *)
	CASE HardwareConfigRead.StopReaction.DriveError.Type OF
		mcASRDE_DEC_LIM:
			BasicConfig.Drive.StopReaction.DriveError	:= mpAXIS_ERROR_STOP_DEC_LIMIT;
		mcASRDE_INDUCT_HALT:
			BasicConfig.Drive.StopReaction.DriveError	:= mpAXIS_ERROR_STOP_INDUCTION;
		mcASRDE_COAST_TO_STANDSTILL:
			BasicConfig.Drive.StopReaction.DriveError	:= mpAXIS_ERROR_STOP_CONTROL_OFF;
	END_CASE
	
	(* Digital Inputs *)
	// Digital inputs not supported
	
END_ACTION
