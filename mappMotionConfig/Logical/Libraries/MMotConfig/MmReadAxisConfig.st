(*********************************************************************************
 * Copyright: B&R Industrial Automation GmbH 
 * Author:    olsonb 
 * Created:   January 27, 2022/5:36 PM 
 * 
 * Reads the configuration of a specific channel of a hardware module and reports
 * the settings in the MpAxisBasicConfigType structure.
 * 
 * The hardware configuration to be read is defined by the AxisLocation input.
 * It contains an axis reference, which is used to read the software configuraiton.
 * These settings are then mapped appropriately throughout BasicConfig.
 *********************************************************************************)

FUNCTION_BLOCK MmReadAxisConfig
		
	// HardwareConfigRead
	ReadHardwareConfig.Name			:= AxisLocation;
	ReadHardwareConfig.Execute		:= Execute;
	ReadHardwareConfig.DataType		:= mcCFG_ACP_AX;
	ReadHardwareConfig.DataAddress	:= ADR(HardwareConfigRead);
	ReadHardwareConfig.Mode			:= mcPCM_LOAD;
	ReadHardwareConfig();	
	
	// SoftwareConfigRead
	ReadSoftwareConfig[0].Component		:= ADR(AxisADR);
	ReadSoftwareConfig[0].Execute		:= Execute;
	ReadSoftwareConfig[0].DataType		:= mcCFG_AX_BASE_TYP;
	ReadSoftwareConfig[0].DataAddress	:= ADR(SoftwareConfigRead.BaseType);
	ReadSoftwareConfig[0].Mode			:= mcPPM_READ;
	ReadSoftwareConfig[0]();
		
	ReadSoftwareConfig[1].Component		:= ADR(AxisADR);
	ReadSoftwareConfig[1].Execute		:= Execute;
	ReadSoftwareConfig[1].DataType		:= mcCFG_AX_MOVE_LIM;
	ReadSoftwareConfig[1].DataAddress	:= ADR(SoftwareConfigRead.MovementLimits);
	ReadSoftwareConfig[1].Mode			:= mcPPM_READ;
	ReadSoftwareConfig[1]();
	
	Error	:= ReadHardwareConfig.Error	OR ReadSoftwareConfig[0].Error	OR ReadSoftwareConfig[1].Error;
	Busy	:= ReadHardwareConfig.Busy	OR ReadSoftwareConfig[0].Busy	OR ReadSoftwareConfig[1].Busy;
	
	// Wait for done
	IF ReadHardwareConfig.Done AND ReadSoftwareConfig[0].Done AND ReadSoftwareConfig[1].Done THEN
		MappConfigToBasicConfig;	// Convert to MpAxisBasicConfig
		Done := TRUE;
	ELSE
		Done := FALSE;
	END_IF
	
END_FUNCTION_BLOCK
