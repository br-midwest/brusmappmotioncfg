
PROGRAM _INIT
    
    MpCnc2Axis_0.Enable := TRUE;

END_PROGRAM

PROGRAM _CYCLIC
    
    (* Instructions for Testing Axis: 
    1.) Load configuration [ConfigFUB_axis1.Load := TRUE] into your axis pv strucutre [ConfigAxis1] to get all "default values" that were configured in the AS project
    2.) Make a change to the axis structure
    3.) Save, to apply the change
    4.) Observe Busy/Done signals, and check if restart is required to apply the change *)
	    
    // Use mappCockpit or [online -> compare -> hardware] to watch changes take effect
    // http://127.0.0.1:8084/mappCockpit/app/index.html
    
    Action_AxisConfigExample;
    
    ConfigFUB_axis1();
    ConfigFUB_axis2();
    ConfigFUB_axis3();
    
    
    (* Instructions for Testing Group: 
    1.) Initialize configuration [MmGroupSettingsInit.Init := TRUE] into your group pv strucutre [ConfigGroup] to get all "default values" that were configured in the AS project
    1b.) Alternatively, load a recipe if you want the recipe to manage all axis group settings.
    2.) Modify a value ConfigGroup
    3.) Set MmGroupConfig_0.Update to apply changes to the axis group configuration and parameters
    4.) If [MmGroupConfig_0.WaitingForGroupPwrOff = TRUE], power off the axis group to finish applying the change to the parameter system.
    5.) Check results using [Online -> Compare -> AutomationComponents] to compare AS vs. Live 
    *)]
    
    Action_GroupConfigExample;
 
    MpCnc2Axis_0();
    MmGroupSettingsInit_0();
    MmGroupConfig_0();
	
END_PROGRAM

PROGRAM _EXIT
    
END_PROGRAM

