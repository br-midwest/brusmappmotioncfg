
ACTION Action_AxisConfigExample: 

    ConfigFUB_axis1.AxisADR	:= ADR(gAxis_1);
    ConfigFUB_axis2.AxisADR	:= ADR(gAxis_2);
    ConfigFUB_axis3.AxisADR	:= ADR(gAxis_3);
	
    ConfigFUB_axis1.BasicConfig	:= ADR(ConfigAxis1);
    ConfigFUB_axis2.BasicConfig	:= ADR(ConfigAxis2);
    ConfigFUB_axis3.BasicConfig	:= ADR(ConfigAxis3);
	
    ConfigFUB_axis1.AxisLocation := 'SL1.IF1.ST1/DriveConfiguration/Channel1';
    ConfigFUB_axis2.AxisLocation := 'SL1.IF1.ST2/DriveConfiguration/Channel1';
    ConfigFUB_axis3.AxisLocation := 'SL1.IF1.ST3/DriveConfiguration/Channel1';
	
    ConfigAxis1.AxisName := 'gAxis_1';
    ConfigAxis2.AxisName := 'gAxis_2';
    ConfigAxis3.AxisName := 'gAxis_3';    
    
    // Axis 2 has a different position limit - no restart required
    ConfigAxis2.Axis.SoftwareLimitPositions.LowerLimit := 0;
    // Axis 3 is a different base type - restart is required
    ConfigAxis3.Axis.BaseType := mpAXIS_LINEAR;

    ConfigFUB_axis1.Enable := TRUE;
    ConfigFUB_axis2.Enable := TRUE;
    ConfigFUB_axis3.Enable := TRUE;
    
END_ACTION

ACTION Action_GroupConfigExample: 
    
    MmGroupSettingsInit_0.GroupName := 'gAxGrp_1';
    MmGroupSettingsInit_0.GroupRef := gAxGrp_1;
    MmGroupSettingsInit_0.GroupSettings := ADR(ConfigGroup);
    
    //MmGroupConfig_0.Update := ;
    MmGroupConfig_0.GroupName := 'gAxGrp_1';
    MmGroupConfig_0.GroupRef := gAxGrp_1;
    MmGroupConfig_0.GroupSettings := ADR(ConfigGroup);
       
    MpCnc2Axis_0.MpLink := ADR(gAxGrp_1);
    MpCnc2Axis_0.Parameters := ADR(MpCnc2AxPar);  
    
END_ACTION
